from bs4 import BeautifulSoup
import os
import rasterio
import numpy as np
import shutil

#converts jp2 file to tiff
#and removes the JP2 file after conversion
def convert_jp2_to_tiff(input_jp2_path, output_tiff_path):
    try:
        # Open the JP2 file
        with rasterio.open(input_jp2_path) as src:
            data = src.read()  # Read all bands
            meta = src.meta.copy()  # Copy the metadata

        # Update metadata for TIFF format
        meta.update(driver='GTiff', dtype=rasterio.float32)

        # Write the data to a new TIFF file
        with rasterio.open(output_tiff_path, 'w', **meta) as dst:
            dst.write(data)
        os.remove(input_jp2_path)
        #print(f"JP2 file '{input_jp2_path}' converted to TIFF: '{output_tiff_path}'")
    except FileNotFoundError:
        print(f"Error: JP2 file '{input_jp2_path}' not found.")
    except Exception as e:
        print(f"Error occurred during conversion of JP2 file '{input_jp2_path}': {e}")


#reads metadata and gets the offest value from toa to boa
def convert_offset_boa(xml_file):
    with open(xml_file, 'r') as file:
        soup = BeautifulSoup(file, 'xml')

    offsets = {}
    for elem in soup.find_all('BOA_ADD_OFFSET'):
        band_id = int(elem['band_id'])
        value = float(elem.text.strip())
        offsets[band_id] = value

    return offsets


#reads metadata and gets the scale factor value from toa to boa
def convert_scale_boa(xml_file):
    with open(xml_file, 'r') as file:
        soup = BeautifulSoup(file, 'xml')

    for elem in soup.find('BOA_QUANTIFICATION_VALUE'):
        value = float(elem.text.strip())
        scalefactor = value
        
    return scalefactor
    
def apply_offsets_and_scale_to_tiff(file, offsets, scalefactor, output_file, band_id):
    with rasterio.open(file) as src:
        profile = src.profile
        image = src.read(1)

        #print(f'apply_offsets_and_scale_to_tiff File:{file}')
        #print(f'apply_offsets_and_scale_to_tiff band_id:{band_id}')
        #print(f'apply_offsets_and_scale_to_tiff offsets:{offsets}')
        #debugger
        max_value = np.max(image)
        if max_value > 10:
            # Create a destination file
            with rasterio.open(output_file, 'w', **profile) as dst:
                band_data = src.read(1)
                # Apply the corresponding offset and scale factor
                if band_id - 1 in offsets:
                    #print(f'band_id - 1:{band_id - 1}')
                    #print(f'offsets[band_id - 1]:{offsets[band_id - 1]}')
                    band_data = (band_data + offsets[band_id - 1]) / scalefactor

                dst.write(band_data.astype(rasterio.float32), 1)
        else: 
            #print(f'It seems like file was allready corrected. No corrections or filters will be applied')   
            with rasterio.open(output_file, 'w', **profile) as dst:
                dst.write(image.astype(rasterio.float32), band_id)	


def main(folder_path, static_path):
	xml_file_path = None
	offsets = None
	scalefactor = None
		
	#create tiff file from jp2 and remove jp2 save original to 		
	for file in os.listdir(folder_path):
		if file.endswith('.jp2'):
				input_jp2_path = os.path.join(folder_path, file)
				output_tiff_path = os.path.join(folder_path, file.replace('.jp2', '.tiff'))
				convert_jp2_to_tiff(input_jp2_path, output_tiff_path)
						
		#extract metadata for correction and offset 
		if file.endswith('.xml'):
			xml_file_path = os.path.join(folder_path, file)
			offsets = convert_offset_boa(xml_file_path)
			scalefactor = convert_scale_boa(xml_file_path)
			
    #check if metadata exists and was properly collected otherwise it fetches default 
    #metadata from static folder and gives a warning the file has to exist in static folder that this works
	try:
		if not offsets:  # This will check if my_var is empty or None
			path_to_static_metadata=os.path.join(static_path, 'STATIC_METADATA_FOR_MISSING_CASES.xml')
			offsets = convert_offset_boa(path_to_static_metadata)
			print(f'ATTENTION: IN {folder_path} NO METADATA FOUND OFFSET NOW DEFAULT -1000 IN THE IMAGE {folder_path}')
	except NameError:
		path_to_static_metadata=os.path.join(static_path, 'STATIC_METADATA_FOR_MISSING_CASES.xml')
		offsets = convert_offset_boa(path_to_static_metadata)
		print(f'ATTENTION: IN {folder_path} NO METADATA FOUND OFFSET NOW DEFAULT -1000 IN THE IMAGE {folder_path}')

	try:
		if not scalefactor:  # This will check if my_var is empty or None
			path_to_static_metadata=os.path.join(static_path, 'STATIC_METADATA_FOR_MISSING_CASES.xml')
			scalefactor = convert_scale_boa(path_to_static_metadata)
			print(f'ATTENTION: IN {folder_path} NO METADATA FOUND SCALE NOW DEFAULT 1000 IN THE IMAGE {folder_path}')
	except NameError:
		path_to_static_metadata=os.path.join(static_path, 'STATIC_METADATA_FOR_MISSING_CASES.xml')
		offsets = convert_scale_boa(path_to_static_metadata)
		print(f'ATTENTION: IN {folder_path} NO METADATA FOUND OFFSET NOW DEFAULT -1000 IN THE IMAGE {folder_path}')

	

	for file in os.listdir(folder_path):				
		#print('NOW IN CORRECTION AND OFFSET APPLIENG LOOP')
        #if checks if object is file or directory continues if it is a file     
		if os.path.isdir(file):
			#print(file)
			continue
		

		#only bands need correction so if it starts with B and ends with .tiff it is a band
		if file.startswith('B') and file.endswith('.tiff'):

			file_path = os.path.join(folder_path, file)
			band_id = int(file[1:3])  # Extract band_id from the filename, e.g., B01.tiff -> 1

			if band_id in offsets:	
				#print('bandid in processes')	           
				orig_dir = os.path.join(folder_path, 'uncorrected_images') #orig dir renamed into uncorrected tiff images
				orig_tiff_path = os.path.join(orig_dir, file)
				if not os.path.exists(orig_dir):
					os.mkdir(orig_dir)

				if not os.path.exists(orig_tiff_path):
					os.rename(file_path, orig_tiff_path)
			            
				output_file_path = os.path.join(folder_path, f"{file}")
				#print('now apply_offsets_and_scale_to_tiff with{offsets} and {scalefactor} and {band_id}')
				apply_offsets_and_scale_to_tiff(orig_tiff_path, offsets, scalefactor, output_file_path, band_id)
				#print(f"Processed {orig_tiff_path} -> {output_file_path}")

		if (file.startswith('CLD') and file.endswith('.tiff')) or (file.startswith('MTD')) :
			#print('CLD or mdt file is used')
			#print(file)
			#print(' ')
			file_path = os.path.join(folder_path, file)	
			orig_dir = os.path.join(folder_path, 'uncorrected_images') #orig dir renamed into uncorrected tiff images
			orig_tiff_path = os.path.join(orig_dir,file)

			if not os.path.exists(orig_dir):
				os.mkdir(orig_dir)
			shutil.copy(file_path, orig_tiff_path)



if __name__ == "__main__":
    functions.remove_dot_config()
    folder_path = '/home/jakob/Dokumente/GIT/linea_de_nieve/Masterarbeit/wrong cloud dedection/test_s2Cloudless/to_correct/'
    main(folder_path, '/home/jakob/Dokumente/GIT/linea_de_nieve/static_layermasks/')

