'''
This script checks for changes from the previous run in the config.yaml file.
If there are changes you can decide if you want to re run the analysis. 
This works with an extra copy of the config file (.config.yaml) to track changes.  This gets always created at the beginning of the analysis
further scripts use the .config.yaml 
'''

import yaml
import os
import functions


def save_yaml(data, file_path):
    # Create the directory if it does not exist
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    
    with open(file_path, 'w') as file:
        yaml.safe_dump(data, file)

#find changed entries between the runs
def get_changed_entries(old_config, new_config):
    changes = {}
    for key, value in new_config.items():
        if key not in old_config or old_config[key] != value:
            changes[key] = value
    return changes





def main():
    
    #load config and .config if available
    config = functions.load_config('config.yaml')
    changed_config_path = os.path.join(config['download_and_processfolder'],'.changed.yaml')

    dot_config_path = os.path.join(config['download_and_processfolder'],'.config.yaml')
    saved_config = None
    if os.path.exists(dot_config_path):
        saved_config = functions.load_config(dot_config_path)

    # Delete old .changed.yaml at the start of each run
    if os.path.exists(changed_config_path):
        os.remove(changed_config_path)

    if saved_config:
        # Check if there are differences
        changed_entries = get_changed_entries(saved_config, config)
        if changed_entries:
            if config['debugging_comments']:
                print("Changes detected. Saving to .changed.yaml and updating .config.yaml.")
            save_yaml(changed_entries, changed_config_path)  # Save only changed entries
            save_yaml(config, dot_config_path)      # Update .config.yaml
            
            yes_choices = ['yes', 'y','si','ja']
            no_choices = ['no', 'n','nein']
            while True:
                user_input = input('Config has been chaged! Do you want that all images get newly processed? Yes/No\n')
                if user_input.lower() in yes_choices:
                    print('Redoing the analysis')
                            
                    with open(dot_config_path, 'r') as file:
                        config = yaml.safe_load(file) 
               

                    # Update the value of 'continous_analysis'
                    config['continous_analysis'] = False

                    # Save the updated configuration back to the file
                    with open(dot_config_path, 'w') as file:
                        yaml.dump(config, file)


                    break
                elif user_input.lower() in no_choices:

                    break
                else:
                    print('Type yes or no')
                    continue
        else:
            if config['debugging_comments']:
                print("No changes detected. Proceeding with existing configuration.")

    else: #if there is no .config.yaml save the config  to the .config (normally fist run of new analysis so there is no changes)
        save_yaml(config, dot_config_path)      # Update .config.yaml

            


if __name__ == "__main__":
    main()
