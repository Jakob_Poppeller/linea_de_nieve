import requests
import json
import os
import requests
import functions
import math

#cordinates of the download are in the topleft corner 
# so lat has the higher value in the topleft corner
def round_lat(value):
    return math.ceil(value / 10) * 10
# and lon has the lower value in the topleft corner
def round_lon(value):
    return math.floor(value / 10) * 10

def adjust_bounds(min_lon, min_lat, max_lon, max_lat):
    # Round bounds to the nearest multiples of the threshold with is 10 
    #because the provided files are multipliers of 10 degrees
    min_lon = round_lon(min_lon)
    max_lon = round_lon(max_lon)
    min_lat = round_lat(min_lat)
    max_lat = round_lat(max_lat)
    
    return min_lon, min_lat, max_lon, max_lat


# Adjust the start of the range to the nearest multiple of the interval homepage needs coordinates in 10 degree steps
def compute_ranges(min_lon, min_lat, max_lon, max_lat, interval):
    start_lat = int((min_lat // interval) * interval)
    start_lon = int((min_lon // interval) * interval)

    # Compute latitude range
    lat_range = range(
        start_lat,  # Start of the range
        int(max_lat + interval),  # End of the range (exclusive, so add interval to include max_lat)
        int(interval)  # Interval
    )

    # Compute longitude range
    lon_range = range(
        start_lon,  # Start of the range
        int(max_lon + interval),  # End of the range (exclusive, so add interval to include max_lon)
        int(interval)  # Interval
    )

    return lat_range, lon_range

# Function to construct the tile URL
def construct_tile_url(lat, lon,base_url):
    lat_str = f'N{abs(lat)}' if lat >= 0 else f'S{abs(lat)}'
    lon_str = f'E{abs(lon)}' if lon >= 0 else f'W{abs(lon)}'
    return f"{base_url}?type=tile&lat={lat_str}&lon={lon_str}"




def main():
    config = functions.load_config()
    if config['debugging_comments']:
        print('\n\n\nStarting script _102_forest_download')
    # Base URL for downloading tiles
    base_url = 'https://ies-ows.jrc.ec.europa.eu/iforce/gfc2020/download.py'

    # Output directory for the downloaded files

    output_dir = os.path.join(config['static_path'],config["forest_folder"])
    os.makedirs(output_dir, exist_ok=True)

    # Define the latitude and longitude ranges and the interval (has to be 10 because of how the homepage provides the files)
    min_lon, min_lat, max_lon, max_lat=functions.get_max_min_bounds(config)
    min_lon, min_lat, max_lon, max_lat=adjust_bounds(min_lon, min_lat, max_lon, max_lat)
    lat_range, lon_range = compute_ranges(min_lon, min_lat, max_lon, max_lat,10)

    # Generate URLs and download each tile
    for lat in lat_range:
        for lon in lon_range:
            url = construct_tile_url(lat, lon, base_url)
            local_filename = os.path.join(output_dir, url.split("lat=")[-1].replace('&lon=', '_').replace('=', '_') + '.tiff')
            if not  os.path.exists(local_filename):
                print('Downlaoding Forest Map of AOI for you...')
                with requests.get(url, stream=True) as r:
                    if r.status_code == 200:
                        with open(local_filename, 'wb') as f:

                            for chunk in r.iter_content(chunk_size=8192):
                                f.write(chunk)

                        print(f"Downloaded: {local_filename}")
                    else:
                        print(f"Failed to download: {url}")
                #print(local_filename)
            else: 
                if config['debugging_comments']:
                    print(f'File {local_filename} allredy exits. Download skipped')
                else: 
                    continue


if __name__ == "__main__":
    functions.remove_dot_config()
    main()

