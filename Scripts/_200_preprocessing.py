import functions
import yaml
import os
import _201_cut_reproject_merge_and_save
import _202_calc_aspect_N_E_S_W
import _203_calc_dem_steps

def main():
    config = functions.load_config()
    dem_folder = os.path.join(config['static_path'], config['dem_downloadfolder'])

    _201_cut_reproject_merge_and_save.main()
    functions.check_and_resize_tifs(os.path.join(config['static_path'],config['processed_masks']),config['pixel_resolution'])
    _202_calc_aspect_N_E_S_W.main()
    _203_calc_dem_steps.main()
    if config['debugging_comments']:
        print('Preprocessing finished!')
    
if __name__ == "__main__":
    functions.remove_dot_config()
    main()
