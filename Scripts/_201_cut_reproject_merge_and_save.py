'''
Script cuts the DEM and Forest files to the specifified bounds, reprojects them, and if they are made out of multiple images they also get merged.

'''
import os
import rasterio
from rasterio.merge import merge
from rasterio.warp import calculate_default_transform, reproject, Resampling, transform_bounds
from rasterio.mask import mask 
from shapely.geometry import box
import numpy as np
from tempfile import TemporaryDirectory
import functions
import yaml


def crop_to_bounds(src, bounds, src_crs, dst_crs, bounds_crs='EPSG:4326'):
    config = functions.load_config()

    # If CRS are different, transform the bounds from dst_crs to src_crs
    if bounds_crs != src_crs:
        if config['debugging_comments']: 
            print(f"Transforming bounds from {bounds_crs} to {src_crs}")
        minx, miny, maxx, maxy = transform_bounds(bounds_crs, src_crs, *bounds)
        if config['debugging_comments']: 
            print(f"Transformed bounds: {minx}, {miny}, {maxx}, {maxy}")
    else:
        if config['debugging_comments']: 
            print(f"Source and destination CRS are the same. No transformation needed.")
        minx, miny, maxx, maxy = bounds

    # Create a bounding box 
    bounds_bbox = box(minx, miny, maxx, maxy)
    
    # Get the source file bounds in the source CRS
    src_bounds = box(*src.bounds)

    # Check if the bounding box intersects with the source file bounds
    if not src_bounds.intersects(bounds_bbox):
        print(f"File {src.name} is not in the specified bounds.\n\n")
        return None, None
    
    # If they intersect, proceed with cropping
    if config['debugging_comments']: 
        print("Cropping to bounds...\n\n")
    geo = [bounds_bbox.__geo_interface__]
    out_image, out_transform = mask(src, geo, crop=True)
    
    # Update the metadata to reflect the new cropped dimensions
    out_meta = src.meta.copy()

    out_meta.update({
        "height": out_image.shape[1],
        "width": out_image.shape[2],
        "transform": out_transform
    })
    
    return out_image[0], out_meta


def process_dems(input_folder, output_path, bounds, dst_crs, chunk_size=1024):
    config = functions.load_config()
    if config['debugging_comments']: 
        print(f"Processing DEMs in folder: {input_folder} ...")

    with TemporaryDirectory() as tmp_dir:
        cropped_files = []
        #print('list of the files\n\n\n', os.listdir(input_folder))
        for filename in os.listdir(input_folder):
            if filename.endswith(".tif") or filename.endswith(".tiff"):
                dem_file = os.path.join(input_folder, filename)
                with rasterio.open(dem_file) as src:
                    #print(f"Cropping file to bounds: {bounds}")
                    
                    cropped_image, cropped_meta = crop_to_bounds(src, bounds, src.crs, dst_crs)
                    # If image was out of bounds, None is returned
                    if cropped_image is not None:  
                        cropped_file_path = os.path.join(tmp_dir, f"cropped_{filename}")
                        with rasterio.open(cropped_file_path, 'w', **cropped_meta) as dest:
                            dest.write(cropped_image, 1)  
                        cropped_files.append(cropped_file_path)

        reprojected_files = []

        for cropped_file in cropped_files:
            with rasterio.open(cropped_file) as src:
                if src.crs.to_string() != dst_crs:
                    if config['debugging_comments']: 
                        print(f"Reprojecting {cropped_file} to {dst_crs}")
                    with rasterio.open(cropped_file) as src:   
                        reprojected_data, reprojected_kwargs = functions.resample_and_reproject(cropped_file, src.meta, dst_crs)
                    reprojected_path = os.path.join(tmp_dir, os.path.basename(src.name))
                    with rasterio.open(reprojected_path, 'w', **reprojected_kwargs) as dst:
                        dst.write(reprojected_data, 1)
                    reprojected_files.append(reprojected_path)
                else:
                    reprojected_files.append(cropped_file)

        # Merge the reprojected files
        src_files_to_mosaic = [rasterio.open(fp) for fp in reprojected_files]
        mosaic, out_trans = merge(src_files_to_mosaic, method='max', nodata=None)

        if mosaic.ndim == 3:
            mosaic = mosaic[0] 

        mosaic[mosaic < 0] = 65535

        out_meta = src_files_to_mosaic[0].meta.copy()
        out_meta.update({
            "driver": "GTiff",
            "height": mosaic.shape[0],
            "width": mosaic.shape[1],
            "transform": out_trans,
            "crs": dst_crs,
            "nodata": 65535
        })

        if out_meta['dtype'] == 'uint8':
            out_meta.update({"nodata": 255})

        with rasterio.open(output_path, "w", **out_meta) as dest:
            dest.write(mosaic, 1)

        if config['debugging_comments']: 
            print(f"Merged and cropped DEM saved to {output_path}")



def main():
    config = functions.load_config()
    if config['debugging_comments']:
        print('\n\n\nStarting script _201_cut_reproject_merge_and_save')

    folders_to_include = [
        config['forest_folder'],
        config['dem_downloadfolder']
        ]
    bounds = functions.get_max_min_bounds(config) 
    crs_string = functions.get_ideal_utm_zone_epsg_for_bounds(config)
    #creates folder
    os.makedirs(os.path.join(config['static_path'],config['processed_masks']),exist_ok=True)
    #an index for labeling if the file is in another folder then dem or forest
    index=0 
    #for loop that changes the fileending corresponding to the kind of inputfile if not in the list it just gets an index
    for folder in folders_to_include: 
        if folder == config['forest_folder']:
            fileending='forest'
        elif folder == config['dem_downloadfolder']:
            fileending='DEM'
        else:
            index += 1
            fileending= str(index)
        output_path = os.path.join(config['static_path'],config['processed_masks'],f"{functions.filename_from_bounds(config)}_{fileending}.tif")

        input_folder = os.path.join(config['static_path'],folder)
        if not os.path.exists(output_path):
            process_dems(input_folder, output_path, bounds, crs_string)
        else: 
            if config['debugging_comments']:           
                print(f'DEM is allready here: {output_path}')

if __name__ == "__main__":
    functions.remove_dot_config()
    main()
