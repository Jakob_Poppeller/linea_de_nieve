'''
Script calculates the aspect of a given DEM it returns a tiff file with aspects
like 
0 = FLat or no data
1 = North 
2 = East
3 = South 
4 = West


'''
import numpy as np
import rasterio
from rasterio.windows import Window
from scipy.ndimage import gaussian_filter
import functions
import yaml
import os



def calculate_aspect(dem):
    #print(dem.shape)

    x, y = np.gradient(dem)
    aspect = np.arctan2(-x, -y) * 180 / np.pi  # Convert to degrees
    aspect = (aspect + 90) % 360  # Adjust so that 0 degrees is North
    return aspect

def calculate_slope(dem, pixel_size):
    #print(dem.shape)

    x, y = np.gradient(dem, pixel_size, pixel_size)
    slope = np.sqrt(x**2 + y**2)
    slope = np.arctan(slope) * 180 / np.pi 
    return slope

def reclassify_aspect(aspect_array):
    aspect_reclassified = np.zeros_like(aspect_array, dtype=np.uint8)
    aspect_reclassified[(aspect_array >= 315) | (aspect_array < 45)] = 1   # North
    aspect_reclassified[(aspect_array >= 45) & (aspect_array < 135)] = 2   # East
    aspect_reclassified[(aspect_array >= 135) & (aspect_array < 225)] = 3  # South
    aspect_reclassified[(aspect_array >= 225) & (aspect_array < 315)] = 4  # West
    return aspect_reclassified

def smooth_dem(dem, sigma=1):
    return gaussian_filter(dem, sigma=sigma)

def main():
    config = functions.load_config()
    if config['debugging_comments']:
        print('\n\n\nStarting script _202_calc_aspect_N_E_S_W')

    bounds = functions.get_max_min_bounds(config) 
    input_filename = f"{functions.filename_from_bounds(config)}_DEM.tif"
    input_path = os.path.join(config['static_path'], config['processed_masks'])
    input_file = os.path.join(input_path, input_filename)

    aspect_filename = f'{input_filename.split(".")[0]}_aspect.tif'
    chunk_size = 1024
    nodata_value = 255  # Setting the nodata value for uint8

    smoothing_sigma = 1  # Standard deviation for Gaussian kernel
    if not os.path.exists(os.path.join(input_path, aspect_filename)):
        # Open the original DEM that is cut into the bounds
        with rasterio.open(input_file) as src:
            width = src.width
            height = src.height
            profile = src.profile.copy()
            profile.update(dtype=rasterio.uint8, count=1, nodata=nodata_value)

            pixel_size_x, pixel_size_y = src.transform[0], -src.transform[4]
            pixel_size = (pixel_size_x + pixel_size_y) / 2  # Average pixel size

            with rasterio.open(os.path.join(input_path, aspect_filename), 'w', **profile) as dst:
                for y in range(0, height, chunk_size):
                    for x in range(0, width, chunk_size):
                        window = Window(x, y, min(chunk_size, width - x), min(chunk_size, height - y))
                        dem_chunk = src.read(1, window=window)
                        
                        # Check the size of the DEM chunk before processing
                        if dem_chunk.shape[0] < 2 or dem_chunk.shape[1] < 2:
                            print(f"Chunk at (x={x}, y={y}) is too small for calculations. Skipping...")
                            # You can write a nodata value or zero to the output for this chunk
                            output_chunk = np.full((min(chunk_size, height - y), min(chunk_size, width - x)), nodata_value, dtype=rasterio.uint8)
                            dst.write(output_chunk, 1, window=window)
                            continue
                        
                        # Apply Gaussian smoothing to the DEM chunk
                        dem_chunk_smoothed = smooth_dem(dem_chunk, sigma=smoothing_sigma)
                        
                        # Calculate slope and mask areas with slope < 15 degrees
                        slope_chunk = calculate_slope(dem_chunk_smoothed, pixel_size)
                        mask = slope_chunk >= 15
                        
                        # Calculate aspect only for areas with slope >= 15 degrees
                        aspect_chunk = calculate_aspect(dem_chunk_smoothed)
                        aspect_chunk = np.where(mask, aspect_chunk, np.nan)  # Mask areas with slope < 15
                        
                        # Reclassify aspect
                        aspect_reclassified_chunk = reclassify_aspect(aspect_chunk)
                        aspect_reclassified_chunk = np.where(mask, aspect_reclassified_chunk, 0)  # Ensure masked areas are 0
                        
                        dst.write(aspect_reclassified_chunk.astype(rasterio.uint8), 1, window=window)
        print('Aspect file saved to:', os.path.join(input_path, aspect_filename))
    else: 
        print(f'Aspect file is already here: {os.path.join(input_path, aspect_filename)}')

                    
if __name__=="__main__":
    functions.remove_dot_config()
    main()
