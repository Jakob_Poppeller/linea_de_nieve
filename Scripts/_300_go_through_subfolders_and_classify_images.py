import os
import _312_convert_jp2_tiff_and_correct 
import _313_calc_NDSI
import _314_Cloudclass
import _315_snowpass
import _316_snowpass2
import yaml
import shutil
import functions
import re




def main():
    config = functions.load_config()
    if config['debugging_comments']:
        print('\n\n\nStarting script _300_go_through_subfolders_and_classify_images')

    downloadfolder = config['download_and_processfolder']
    static_folder = config['static_path']


    for folder_name in os.listdir(downloadfolder):

        folder_path = os.path.join(downloadfolder, folder_name)
        # Check if it's a directory (subfolder)
        if os.path.isdir(folder_path):  
            if config['debugging_comments']:
                print('Current folder:',folder_path)  


            #Check if uncorrected_images exists and if it exists unprocessed files get copied to original image folder

            uncorr_path=os.path.join(folder_path, 'uncorrected_images')

            #if uncorrected_images_folder exists (that means it was allready processed or started to process) the original state gets restored by copying 
            #the uncorrected images back to the folder 
            #but with continous analysis this gets skipped 

            changed_path = os.path.join(config['download_and_processfolder'], '.changed.yaml')
            config_changes = functions.load_config(changed_path) if os.path.exists(changed_path) else None


            if os.path.exists(uncorr_path) and config_changes:
                functions.get_uncorrected_images_back(uncorr_path, folder_path)

            #date pattern to identify the right folders
            date_pattern = r"^\d{4}-\d{2}-\d{2}T\d{2}-\d{2}-\d{2}\.\d{6}Z.*"
            if re.match(date_pattern, folder_name):

                folder_date=folder_name.split('T')[0]
                start_date = config['start_date'] 

                if config['continous_analysis']:
                    start_date, end_date = functions.get_start_and_endate(config, downloadfolder)

                if config['debugging_comments']:    
                    print("Folder date:", folder_date)
                    print("Start date:", start_date)

                if config['continous_analysis'] and folder_date < start_date:
                    if config['debugging_comments']:
                        print('Folder ',folder_path,' skipped it was already processed. For this analysis')
                    continue
                     #< not <= so that new images of this day gets processed last image gets checked if uncorrected images exist in the folder see next lines.
                

                files = os.listdir(folder_path)
                epsg = (functions.get_ideal_utm_zone_epsg_for_bounds(config).split(':')[1])

                #check if the snowpass2 file exists
                target_file_exists = any(file.endswith(f"{epsg}_{config['snowpass_2_name']}") for file in files)
                freshly_downloaded_files_exists = any(file.endswith(f".jp2") for file in files)
                #if checks if there are snowpass2 file (that means it was processed, or if file could be deleded or if there is only the downolads (freshly jp2 files))
                if not os.listdir(folder_path):  # Directory is empty
                    os.rmdir(folder_path)  # Remove the empty directory it gets redownlaoded if folder does not exist in the next run
                    print(f"Removed empty directory: {folder_path}")
                    continue 
                if not target_file_exists and not config['delete_downloaded_folders_after_merging'] and not freshly_downloaded_files_exists:
                    print('Restoring unprocessed images in', folder_path)
                    functions.get_uncorrected_images_back(uncorr_path, folder_path)
                    
                
            
                #print(f'convert_jp2_tiff_and_correct now with {folder_path}')
                _312_convert_jp2_tiff_and_correct.main(folder_path, static_folder)
                #print(f"Conversion from .jp2 to .tiff done! (if there was any) And bands corrected in folder : {folder_path}")



                # Check for the required files in the current folder 
                # If all required files exist nothing happens 
                # else continue with next folder and writes what is missing
                missing_files = functions.check_files_exist(folder_path, config['required_files'])
                if missing_files: 
                    continue

                #claculate the NDSI and save it in the current Folder
                _313_calc_NDSI.main(folder_path)
                #print(f"NDSI calculated in folder: {folder_path}")
                
                _314_Cloudclass.main(folder_path,config['cld_threshold_cloudclass_2'],config['cld_threshold_cloudclass_1'],config['nir_threshold_cloudclass_1'],config['cloudclass_file_name'])
                #print(f"Cloudclass calculated in folder: {folder_path}")
                            

                snowpass_1 = _315_snowpass.main(folder_path, config['threshold_value_red_sp1'], config['threshold_value_NDSI_sp1'], config['snowpass_1_name'])
                #print(f"{config['snowpass_1_name']} calculated in folder: {folder_path}")


                snowpass_2 = _316_snowpass2.main(folder_path)
                #print(f"{config['snowpass_2_name']} calculated in folder: {folder_path}")

                #Cleanup of not needed files if said so in config
                if config['clean']:
                    snowpass_2 = functions.delete_processing_files(folder_path, config['keep_files'])
                    #print(f"{folder_path} cleaned")


                functions.append_date_time_prefix(folder_path)
                #print(f"{folder_path} renamed")

                functions.reproject_sp2_files(folder_path)
            else:
                if config['debugging_comments']:
                    print('Correct date but already processed. Folder:', folder_path)

               
        else:
            #if file is not a folder skip.
            continue


    print('Done with the calssification of the satellite images!')
 

if __name__ == "__main__":
    functions.remove_dot_config()
    main()

