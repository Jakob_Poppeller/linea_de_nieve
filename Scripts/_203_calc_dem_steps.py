import numpy as np
import rasterio
from rasterio.windows import Window
import functions
import yaml
import os
import math

def round_array(arr, stepsize, round_down):
    if stepsize == 0:
        raise ValueError("stepsize cannot be zero")
    if round_down:
        return np.floor(arr / stepsize) * stepsize
    else:
        return np.round(arr / stepsize) * stepsize

def main():
    config = functions.load_config()
    if config['debugging_comments']:
        print('\n\n\nStarting script _203_calc_dem_steps')


    bounds = functions.get_max_min_bounds(config) 
    input_filename = f"{functions.filename_from_bounds(config)}_DEM.tif"
    input_path = os.path.join(config['static_path'], config['processed_masks'])
    input_file = os.path.join(input_path, input_filename)
    
    stepsize = config['elev_stepsize']
    round_down = config['round_down']

    elev_filename_steps = f'{input_filename.split(".")[0]}_elev_{stepsize}_m_steps.tif'
    chunk_size = 1024
    nodata_value = 65535  # Setting the nodata value for uint16
    changed_path = os.path.join(config['download_and_processfolder'],'.changed.yaml')
    re_run = False
    if os.path.exists(changed_path):
        config_changes = functions.load_config(changed_path)
        if 'elev_stepsize' in config_changes:
            stepsize = config_changes['elev_stepsize']
            re_run = True
        if 'round_down' in config_changes:
            round_down = config_changes['round_down']
            re_run = True


    if not os.path.exists(os.path.join(input_path, elev_filename_steps)) or re_run is True:
        # Open the original DEM that is cut into the bounds
        with rasterio.open(input_file) as src:
            width = src.width
            height = src.height
            profile = src.profile.copy()
            profile.update(dtype=rasterio.uint16, count=1, nodata=nodata_value)

            with rasterio.open(os.path.join(input_path, elev_filename_steps), 'w', **profile) as dst:
                for y in range(0, height, chunk_size):
                    for x in range(0, width, chunk_size):
                        window = Window(x, y, min(chunk_size, width - x), min(chunk_size, height - y))
                        dem_chunk = src.read(1, window=window)
                        #print(dem_chunk)
                        stepped_chunk = round_array(dem_chunk, stepsize, round_down)
                        #print(stepped_chunk) 
                        dst.write(stepped_chunk.astype(rasterio.uint16), 1, window=window)
    else: 
        print(f'Elevation rounded file is already here: {os.path.join(input_path, elev_filename_steps)}')




if __name__ == "__main__":
    functions.remove_dot_config()
    main()
