import rasterio
import numpy as np
import os
from rasterio.warp import calculate_default_transform, reproject, Resampling
import functions 


    

def main(folder_path,cld_threshold_2,cld_threshold_1,nir_threshold_1,output_name):
    #print(f'folderpath is at cloudclass start:{folder_path}')
    quality_cloud_confidence = os.path.join(folder_path, 'CLD.tiff')
    nir_b8 = os.path.join(folder_path, 'B08.tiff')
    output_file_name = os.path.join(folder_path,output_name)

    # Open quality_cloud_confidence band
    with rasterio.open(quality_cloud_confidence) as qcc_src:
        qcc = qcc_src.read(1)  
        
    # Open NIR band
    with rasterio.open(nir_b8) as nir_src:
        nir = nir_src.read(1)  
        meta_nir = nir_src.meta.copy()



    if qcc.shape != nir.shape:
            qcc, kwargs = functions.resample_and_reproject(quality_cloud_confidence, meta_nir)

    # Calculate Cloud_class
    cloud_class = np.where(
        qcc > cld_threshold_2, 2,
        np.where(
        (qcc > cld_threshold_1) & (nir > nir_threshold_1), 1, 0
        )
    )




    # Write the result to a new file
    with rasterio.open(output_file_name, 'w', **kwargs) as dst:
        dst.write(cloud_class.astype(rasterio.uint8), 1)
    #print(f'folderpath is at cloudclass end:{folder_path}')
    #print(f'outputfilename is at cloudclass end:{output_file_name}')
    output_file_name

    print(f"Cloud_Class calculation complete. Output saved to {folder_path+output_file_name}")

if __name__ == "__main__":
    functions.remove_dot_config()
    main(folder_path)
