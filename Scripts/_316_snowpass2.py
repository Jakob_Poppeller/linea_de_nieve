import os
import rasterio
import numpy as np
from rasterio.warp import reproject, Resampling
from rasterio.windows import from_bounds
from rasterio.transform import array_bounds
import _315_snowpass as snowpass
import yaml
import functions

def percentage_and_threshold_snowcover(inputfile_mask, input_snowpass1, folder_path, threshold_value):
    #inputfile_mask is the 100m elevation file without clouds (value 9004) cropped to the area of intrest

    snowcover_pass1_info_file = os.path.join(folder_path, 'snowcover_pass1_info_file.txt') 

    filtered_values= inputfile_mask[(input_snowpass1 != 0) & (inputfile_mask > 0) & (inputfile_mask < 9000)] 
    #exclude parts that are no satelite image 
    #inputfile_mask > 10 because nodata is 0 clouds are now 9004 

    unique_values = np.unique(filtered_values) 
    #writes all unique values down


    percentages = {}


    for value in unique_values:
        # Create a mask where inputfile_mask equals the current value and input_snowpass1 snow or no snow but not nodata
        count_value = (inputfile_mask == value) & (input_snowpass1 != 0)
    
        # Debugging: Print intermediate masks
        #print(f"Value: {value}")
        #print("count_value mask:")
        #print(count_value)
    
     # Total pixels meeting the count_value condition (with elevation and either snow or no snow)
        total_pixels = np.sum(count_value)
        #print(f"Total pixels where inputfile_mask == {value} and input_snowpass1 != 9: {total_pixels}")

    
    # Count pixels where both count_value and input_snowpass1 == 2 are true
        count_snowpass1 = np.sum(count_value & (input_snowpass1 == 2))
    
        if total_pixels > 0:
            percentage = (count_snowpass1 / total_pixels) * 100
        else:
            percentage = 0.0  # I case of division by zero 

        percentages[value] = percentage

    with open(snowcover_pass1_info_file, 'w') as f:
        for value, percentage in percentages.items():
            f.write(f"Altitude {value}: {percentage:.2f}%\n")

    sorted_percentages = sorted(percentages.items(), reverse=True)
    last_value_above_threshold = None

    for value, percentage in sorted_percentages:
        if percentage > threshold_value:
            last_value_above_threshold = value
        else:
            break

    if last_value_above_threshold is not None:
        print(' ')
        print(f"The last Elevation above {threshold_value}% Snowcover is: {last_value_above_threshold}")
        print(' ')
    else:
        print(f"No elevation found above {threshold_value}% Snowcover")
        print('PROBABLY NO OR BROKEN DEM? NO SNOWPASS2 WILL BE CALCULATED! SNOWPASS1 WILL BE USED!')
        last_value_above_threshold = 9000 #value so high that no snowpass2 run gets appied if there is no snow.
                                           # Otherwise rivers etc. are classified as snow 
    return last_value_above_threshold



def match_shapes(data1, data2, nodata_value):
    if data1.shape != data2.shape:
        min_height = min(data1.shape[0], data2.shape[0])
        min_width = min(data1.shape[1], data2.shape[1])

        # Trim both arrays to the minimum size
        data1_trimmed = data1[:min_height, :min_width]
        data2_trimmed = data2[:min_height, :min_width]

        # Create a new array to store the result
        matched_data = np.full((min_height, min_width), nodata_value, dtype=data1.dtype)

        # Copy the trimmed data into the result array
        matched_data[:, :] = data1_trimmed
    else:
        matched_data = data1

    return matched_data


# Function to handle reprojection and resampling, and then masking out nodata values
def resample_reproject_and_cut_to_aoi(input_path, sp1_data, sp1_profile, nodata_value, dtype):
    with rasterio.open(input_path) as src:
        input_data = src.read(1, masked=False)

        # Calculate the target transform and dimensions
        target_transform = sp1_profile['transform']
        target_height, target_width = sp1_profile['height'], sp1_profile['width']

        # Prepare an empty array to hold the resampled data
        resampled_data = np.empty((target_height, target_width), dtype=dtype)

        # Perform the reprojection and resampling
        reproject(
            source=input_data,
            destination=resampled_data,
            src_transform=src.transform,
            src_crs=src.crs,
            dst_transform=target_transform,
            dst_crs=sp1_profile['crs'],
            resampling=Resampling.nearest,
            dst_nodata=nodata_value
        )

        # Ensure the cropped_data array has the same shape as sp1_data
        cropped_data = np.zeros_like(sp1_data, dtype=dtype)

        # Exclude nodata value
        no_nodata_mask = resampled_data != nodata_value

        # Apply the mask
        cropped_data[no_nodata_mask] = resampled_data[no_nodata_mask]

        # Update the profile for saving the output file
        output_profile = sp1_profile.copy()
        output_profile.update({
            'count': 1,
            'dtype': dtype
        })

    return cropped_data, output_profile



def main(folder_path):

    config = functions.load_config()

    #static masks:
    masks_path = os.path.join(config['static_path'],config['processed_masks'])
    for filename in os.listdir(masks_path):
        if filename.startswith(functions.filename_from_bounds(config)) and filename.endswith('_m_steps.tif'):
            elevation_file_steps = os.path.join(masks_path,filename)
    forest_mask = os.path.join(config['static_path'],config['processed_masks'],f"{functions.filename_from_bounds(config)}_forest.tif")

    #filepath for the elevation file cut for this specific image no clouds and if selected no forest 
    output_file_elev_mask=os.path.join(folder_path,'cropped_elev_mask_processed.tiff' )
    #filepath for the forset file cut for this specific image
    output_file_forest=os.path.join(folder_path,'cropped_mask_forest.tiff' )
    #other paths
    snowpass_1_file = os.path.join(folder_path, config['snowpass_1_name'])
    cloud_class_file = os.path.join(folder_path,config['cloudclass_file_name'])
    snowpass2_file = os.path.join(folder_path, config['snowpass_2_name'])


    #test if static files exist
    try:
        elevation_file_steps
    except NameError:
        print(f"ERROR: Elevation steps file is missing in {masks_path}.")
        quit()
    try:
        forest_mask
    except NameError:
        print(f"ERROR: Forrest mask file is missing in {masks_path}.")
        quit()



    #snowpass 1 also serves as the area of intrest because it is cut allready with right borders
    with rasterio.open(snowpass_1_file) as src:
        sp1_profile = src.profile
        sp1_data = src.read(1, masked=True).astype(np.uint8)
        sp1_transform = src.transform

    with rasterio.open(cloud_class_file) as src:
        cloud_profile = src.profile
        cloud_data = src.read(1, masked=True).astype(np.uint8)
        cloud_transform = src.transform



    
    # resample and reproject forest data
    processed_forest_data,processed_forest_profile = resample_reproject_and_cut_to_aoi(forest_mask, sp1_data,  sp1_profile, nodata_value=255, dtype=np.uint8)

    # resample and reproject mask data
    processed_elev_steps_data,processed_elev_steps_profile=resample_reproject_and_cut_to_aoi(elevation_file_steps, sp1_data, sp1_profile, nodata_value=65535, dtype=np.uint16)

    #excludes forest and clouds from calculation of the elevation threshold
    processed_elev_steps_data[processed_forest_data == 1] = 9005 #higher as highest elevation -> 9005 = Forest value 
    processed_elev_steps_data[cloud_data >= config['cld_switch_for_final_product']] = 9004 #higher as highest elevation -> 9004 = value 

   
    with rasterio.open(output_file_elev_mask, 'w', **processed_elev_steps_profile) as dst:
        dst.write(processed_elev_steps_data, 1)

    with rasterio.open(output_file_forest, 'w', **processed_forest_profile) as dst:
        dst.write(processed_forest_data, 1)
    


    threshold_value = percentage_and_threshold_snowcover(processed_elev_steps_data, sp1_data, folder_path, config['threshold_percentage_snow_for_no_snow'])

  #snowpass2 for whole area of intrest. Note: the snowpass saves the file and later on it gets opened again
    snowpass.main(folder_path, config['threshold_value_red_sp2'], config['threshold_value_NDSI_sp2'], config['snowpass_2_name'])
  
  # Open snowpass2
    with rasterio.open(snowpass2_file) as snowpass2_src:
        sp2_data = snowpass2_src.read(1, masked=True).astype(np.uint8)
        sp2_profile = snowpass2_src.profile

 
    # Check if the dimensions match
    if processed_forest_data.shape != sp1_data.shape:
        raise ValueError("The shape of 'forest' does not match the shape of 'sp1_data'")

  
    if sp2_data.shape != sp1_data.shape:
        raise ValueError("The shape of 'sp2_dats' does not match the shape of 'sp1_data'")


    if processed_elev_steps_data.shape != sp1_data.shape:
        raise ValueError("The shape of 'modified_mask' does not match the shape of 'sp1_data'")

    high_mask = processed_elev_steps_data >= threshold_value

     # Set values to 0 where high_snow_mask is False
    sp2_data[~high_mask] = 0
    sp2_data[~high_mask] = sp1_data[~high_mask]
    sp2_data[processed_elev_steps_data == 9004] = 4 #overwrites clouds (and if activated forest) with 4 (index for clouds) but in next line forest gets overwritten with 3 so the output is right
    sp2_data[(processed_forest_data == 1) & (sp2_data != 2)] = 3 #forest overwrites everything exept snow keep in mind that for calculation of the thershold between the elevation of sp1 and sp2 only open areas are used!


    with rasterio.open(snowpass2_file, 'w', **sp2_profile) as dst:
        dst.write(sp2_data, 1)

    # Extract CRS as a string
    crs_string = sp2_profile['crs'].to_string()
    
    crs_string_clean = crs_string.replace(':', '_').replace('/', '_').replace(' ', '_')
    
    #path with CRS in the filename
    new_file_path = os.path.join(folder_path, f"{crs_string_clean}_{config['snowpass_2_name']}")

    # Rename file
    os.rename(snowpass2_file, new_file_path)
    #print(f"File has been renamed to '{new_file_path}'")


if __name__ == "__main__":
    functions.remove_dot_config()

    #folder_path = ''
    main(folder_path)
