## Functions Script 
import os
import re
import glob
import yaml
import geopandas as gpd
from shapely.geometry import box
from math import floor
import rasterio
import numpy as np
from rasterio.warp import calculate_default_transform, reproject, Resampling
from rasterio.merge import merge
from datetime import datetime
import sys
import shutil

#
#
# GENERAL FUNCTIONS
#
#
def load_config(file_name=None):
    # if function calls with a filename open this name
    if file_name:
        if os.path.exists(file_name):
            with open(file_name, 'r') as file:
                return yaml.safe_load(file)
        else:
            raise FileNotFoundError(f"{file_name} does not exist.")
    
    #Else: check for '.config.yaml' first, then 'config.yaml'
    if os.path.exists('.config.yaml'):
        with open('.config.yaml', 'r') as file:
            return yaml.safe_load(file)
    elif os.path.exists('config.yaml'):
        with open('config.yaml', 'r') as file:
            return yaml.safe_load(file)
    
    # Return empty config if no files are found
    return {}
#
def remove_dot_config():
    if os.path.exists('.config.yaml'):
        os.remove('.config.yaml')


#
#
def get_start_and_endate(config, directory):
    '''
    Extracts start and enddate from the config. 
    But if there is continous analysis and there is allready a 
    Snowpass2 file in the right bounds it will use the latest date of this file as the startdate instead.
    The enddate is the enddate from the config file unless end_date_is_today is true.
    '''
    yes_choices = ['yes', 'y','si','ja']
    no_choices = ['no', 'n','nein']
    bounds = filename_from_bounds(config)
    start_date=config['start_date']
    end_date=config['end_date']
    epsg = (get_ideal_utm_zone_epsg_for_bounds(config).split(':')[1])
    bounds_name= filename_from_bounds(config)
    for file in os.listdir(directory):
        filename_pattern =  re.compile(r"^\d{4}-\d{2}-\d{2}"+f"_EPSG_{epsg}_{bounds_name}_merged_snowpass_2\.tiff$")
        if filename_pattern.match(file):
            if config['continous_analysis']: 
                start_date=file.split('_')[0]
            else: 
                print('')
                print('')
                print('')
                while True:
                    user_input = input('There is allready a Snowpass2 file with the same bounds and continous analysis is disabled, so it probably only regenerates this snowpassfile, unless some other settings are changed. Do you want to redo the analysis and continue? \n\nINFO: Redoing the analysis mans that all snowpass and statistic files get removed and the analysis tarts over with the classifacation of the satelite images!\n')

                    if user_input.lower() in yes_choices:
                        print('Redoing the analysis')
                        start_date=config['start_date']
                        delete_files_with_same_date_in_folder(directory)
                        break
                    elif user_input.lower() in no_choices:
                        print('Terminating Analysis')
                        sys.exit()
                    else:
                        print('\nType yes or no\n')
                        continue

    if config['end_date_is_today']:
        end_date=datetime.today().strftime('%Y-%m-%d')
    if config['debugging_comments']:
        print('FOR DOWNLAODS: \ncalculated start_date:',start_date,'\nconfig start_date:',config['start_date'],'\nclaculated end_date:',end_date,'\nconfig end_date',config['end_date'],) 
    return start_date, end_date

#
#
#
def check_files_exist(folder_path, file_list):
# Checks with an array of filenames and a path wich files are missing and returns a list of missing files

    missing_files = []
    
    for file_name in file_list:
        # Construct the full file path
        file_path = os.path.join(folder_path, file_name)
        
        # Check if the file exists, if not, add to the missing files list
        if not os.path.isfile(file_path):
            missing_files.append(file_name)

    if missing_files:
        # If there are missing files, print the message and continue to the next folder
        print(f"Folder '{folder_path}' is missing the following files: {', '.join(missing_files)}")
        print(' ')
        print(' ')
    return missing_files
#
#
#
def delete_processing_files(directory, exclude_filenames):
#Removes all .tiff files in the specified directory except for the ones listed in exclude_filenames.

    # Ensure exclude_filenames contains only the base filenames
    exclude_filenames = set(os.path.basename(filename) for filename in exclude_filenames)
    
    # List all .tiff files in the specified directory
    tiff_files = glob.glob(os.path.join(directory, '*.tiff'))
    
    for tiff_file in tiff_files:
        # Get the base filename of the current tiff file
        tiff_filename = os.path.basename(tiff_file)
        
        # Check if the file should be excluded
        if not any(tiff_filename.endswith(suffix) for suffix in exclude_filenames):
            try:
                os.remove(tiff_file)
                print(f"Removed: {tiff_file}")
            except Exception as e:
                print(f"Error removing {tiff_file}: {e}")

#
#
#
def append_date_time_prefix(folder_path):
    # Extract the date and time from the folder name
    folder_name = os.path.basename(folder_path)
    #print(folder_name)
    date_time_str = folder_name.split('.')[0]  # Date and time must be the first part of the folder name
    #print(f"Date and time extracted: {date_time_str}")
    
    # Iterate over all files in the folder
    for filename in os.listdir(folder_path):
        # Construct the full file path
        file_path = os.path.join(folder_path, filename)
        
        # Skip directories, process only files
        if os.path.isfile(file_path):
            #print(f"Processing file: {file_path}")
            #works till year 2099
            #should never be activated if scripts run together
            #because old files get removed or moved first depending on settings but if run as standalone it does not remove old processed files
            if filename.startswith('20'):
                print(f"Filename to be marked old: {filename}")
                new_filename = f"{filename[:-5]}_old.tiff"
                new_file_path = os.path.join(folder_path, new_filename)
            else:
                # Construct the new filename with the date and time prefix
                new_filename = f"{date_time_str}_{filename}"
                new_file_path = os.path.join(folder_path, new_filename)
                
            # Rename the file
            os.rename(file_path, new_file_path)
            #print(f"Renamed '{filename}' to '{new_filename}'")
    
    print("All files have been renamed with date and time prefix.")
#
#
#
def list_files_with_prefix(root_dir, prefix):
    """List all files in root_dir and subdirectories that start with the given prefix."""
    matching_files = []
    
    # Traverse the directory and subdirectories
    for dirpath, _, filenames in os.walk(root_dir):
        for filename in filenames:
            if filename.startswith(prefix):
                # Store the full path of the matching file
                full_path = os.path.join(dirpath, filename)
                matching_files.append(full_path)
    
    return matching_files
#
#
#

def display_files(files):
    """Display the files with numbering."""
    if not files:
        print("No files found with the specified prefix.")
        return None
    
    for idx, file in enumerate(files, start=1):
        print(f"{idx}: {os.path.basename(file)}")
    
    return True

#
#
#

def choose_file(files):
    """Prompt the user to choose a file by number."""
    while True:
        try:
            choice = int(input("Enter the number of the file you want to choose: ")) - 1
            if 0 <= choice < len(files):
                return files[choice]
            else:
                print("Invalid choice, please try again.")
        except ValueError:
            print("Please enter a valid number.")
#
#
#
def extract_latest_date(file_list):
#function that extracts the latest date of a given file list the filename has to start like /xxx/yyy/YYY-MM-DDT...
    config = load_config()
    latest_date = datetime.strptime(config['start_date'], '%Y-%m-%d')
    if file_list:
        for file_path in file_list:
            # Extract the date part of the filename
            segments = file_path.split('/')
            date_str = segments[-1].split('T')[0]  # Extracts the date part '2024-04-02'
            
            # Convert the date string to a datetime object
            date_obj = datetime.strptime(date_str, '%Y-%m-%d')
            
            # Update the latest_date if this date is more recent
            if latest_date is None or date_obj > latest_date:
                latest_date = date_obj
    
    # Convert the latest_date back to a string
    return latest_date.strftime('%Y-%m-%d')

#
#
#
def dates_in_between(date1_str, date2_str):
#returns the number of dates in between two given dates in 
    date1 = datetime.strptime(date1_str, "%Y-%m-%d")
    date2 = datetime.strptime(date2_str, "%Y-%m-%d")
    
    # Calculate the absolute difference in days
    difference = abs((date2 - date1).days)
    
    
    return difference
#
#
#
#restores the uncoreected images from uncorr_path in folder_path
def get_uncorrected_images_back(uncorr_path, folder_path):
    config=load_config()
    for file in os.listdir(uncorr_path):

        file_to_copy_from = os.path.join(uncorr_path, file)
        file_to_copy_to = os.path.join(folder_path, file)

        old_files_list = os.listdir(folder_path)
        
        for old_file in old_files_list:
            
            #removes all files that are allready in uncorrected images and will be replaced by the uncorrected images
            if old_file.endswith(file):
                old_file_path_inputs = os.path.join(folder_path, old_file)                            
                os.remove(old_file_path_inputs)
                continue
            #checks for four digits at the beginning of the filename
            if re.match(r'^\d{4}', old_file): 
                old_file_path_processed = os.path.join(folder_path, old_file)

                if config['save_files_from_last_run']:
                    destination_folder=folder_path+'/processed_files_from_last_run/'
                    new_file_path = os.path.join(destination_folder, old_file)

                    os.makedirs(destination_folder, exist_ok=True)
                    shutil.move(old_file_path_processed, new_file_path)

                else:
                    os.remove(old_file_path_processed)

        os.system(f'cp "{file_to_copy_from}" "{file_to_copy_to}"')

#####################################################################################################################
# Bounding box and Coordinate functions
#####################################################################################################################
#
#
#
def get_bounding_box(config):
#Get the bounding Box for min_lon, min_lat, max_lon, max_lat Data from the Config file:
    use_custom_bounds = config.get("use_custom_bounds", False)
    
    if use_custom_bounds:
        custom_bounds = config.get("custom_bounds")
        if all(value is not None for value in custom_bounds.values()):
            min_lat = custom_bounds["min_lat"]
            min_lon = custom_bounds["min_lon"]
            max_lat = custom_bounds["max_lat"]
            max_lon = custom_bounds["max_lon"]
            return convert_bbox(min_lon, min_lat, max_lon, max_lat)
        else:
            raise ValueError("Custom bounds specified but some values are missing")
    
    selected_region = config.get("selected_region")
    if selected_region in config.get("regions"):
        min_lon, min_lat, max_lon, max_lat = config["regions"][selected_region]
        return convert_bbox(min_lon, min_lat, max_lon, max_lat)
    
    raise ValueError("No valid bounding box specified in the configuration")
#
#
#
def convert_bbox(min_lon, min_lat, max_lon, max_lat):
#Convert the bounding from min_lon, min_lat, max_lon, max_lat to a bounding box (function is used in get_bounding_box)    

    bounding_box = [
        [min_lon, max_lat],
        [max_lon, max_lat],
        [max_lon, min_lat],
        [min_lon, min_lat],
        [min_lon, max_lat]
    ]
    return bounding_box
#
#
#
def get_max_min_bounds(config):
#returns the bounds as 4 variables
    use_custom_bounds = config.get("use_custom_bounds", False)
    
    if use_custom_bounds:
        custom_bounds = config.get("custom_bounds")
        if all(value is not None for value in custom_bounds.values()):
            min_lat = custom_bounds["min_lat"]
            min_lon = custom_bounds["min_lon"]
            max_lat = custom_bounds["max_lat"]
            max_lon = custom_bounds["max_lon"]
            return min_lon, min_lat, max_lon, max_lat
        else:
            raise ValueError("Custom bounds specified but some values are missing")
    
    selected_region = config.get("selected_region")
    if selected_region in config.get("regions"):
        min_lon, min_lat, max_lon, max_lat = config["regions"][selected_region]
        return min_lon, min_lat, max_lon, max_lat
    
    raise ValueError("No valid bounding box specified in the configuration")
#
#
#
def get_ideal_utm_zone_epsg_for_bounds(config):
#Determine the appropriate UTM zone EPSG code based on bounding box coordinates. 
#WARNING: Hemisphere is Hardcoded to North -> adapt or change if it is on southern hemisphere
    minx, miny, maxx, maxy=get_max_min_bounds(config)


    # Calculate the center longitude of the bounding box
    center_longitude = (minx + maxx) / 2.0

    # Calculate the UTM zone number
    utm_zone = floor((center_longitude + 180) / 6) + 1

    # Defines the hemisphere (N for northern hemisphere)
    hemisphere = 'N'

    # Construct the EPSG code
    epsg_code = f'EPSG:326{utm_zone:02d}' #{hemisphere}' this part must be added if there is also areas in the southern hemisphere
    
    return epsg_code
#
#
#
def filename_from_bounds(config):
#Constructs the filename used in this project for certain bounds
    bounds = get_max_min_bounds(config) 
    filename= f"{int(bounds[0])}_{int(bounds[1])}_{int(bounds[2])}_{int(bounds[3])}"
    return filename
#
#
#
import geopandas as gpd
from shapely.geometry import Polygon

def is_geometry_in_bounds(geometry):
    # Load configuration and extract the expected CRS
    config = load_config()
    crs = get_ideal_utm_zone_epsg_for_bounds(config).split(':')[1]
    
    # Get the bounding box in the correct CRS
    min_lon, min_lat, max_lon, max_lat = get_max_min_bounds(config)
    bbox = convert_bbox(min_lon, min_lat, max_lon, max_lat)  # Ensure this is in the same CRS
    bbox_geo = gpd.GeoSeries([Polygon(bbox)], crs="EPSG:4326")  # Create a polygon for the bbox
    bbox_projected = bbox_geo.to_crs(epsg=crs)  # Reproject the bounding box to the desired CRS
    
    # Get the reprojected bounding box coordinates
    minx, miny, maxx, maxy = bbox_projected.total_bounds
    print('Projected Bounds:', minx, miny, maxx, maxy)
    
    for coordinate in geometry:
        # Reproject the geometry to the target CRS (if not already in the same CRS)


        # Get the geometry bounds
        geom_minx, geom_miny, geom_maxx, geom_maxy = coordinate.bounds
        print('Microregion Bounds:', geom_minx, geom_miny, geom_maxx, geom_maxy)

        # Check if the geometry bounds overlap with the bounding box
        if not (geom_maxx < minx or  # Completely to the left
                geom_minx > maxx or  # Completely to the right
                geom_maxy < miny or  # Completely below
                geom_miny > maxy):   # Completely above
            return True  # There is an overlap
    
    return False  # No overlap found

'''
def find_country_for_bounds():
#not used yet can be used to find country dems from https://sonny.4lima.de/ but downloads are with google and it still has issues 
#one option would be: just returning the list and download the coutries manually
    config = load_config()
    static_path=config['static_path']
    countries_folder=config['folder_countries']
    print(static_path)
    countries_shapefile = os.path.join(static_path,countries_folder)
    # Load the countries shapefile
    gdf_countries = gpd.read_file(countries_shapefile)

    # Create a bounding box geometry
    minx, miny, maxx, maxy = get_max_min_bounds(config)
    bbox = box(minx, miny, maxx, maxy)

    # Check which country(s) the bounding box intersects
    intersecting_countries = gdf_countries[gdf_countries.geometry.intersects(bbox)]
    
    if intersecting_countries.empty:
        print("No country found for the given bounds.")
    else:
        print("Countries intersecting with the bounds:")
        print(intersecting_countries[['NAME']])  # Adjust the column name based on your shapefile's attribute names


    return intersecting_countries[['NAME']]
'''
#
#
#
#####################################################################################################################
# Processing Functions
#####################################################################################################################
#
#
#
def resample_and_reproject(source_path, target_meta, target_crs=None):
    if target_crs is None:
        target_crs = target_meta['crs']  # Use CRS from target_meta if no CRS is provided

    with rasterio.open(source_path) as src:
        print( target_meta['width'], target_meta['height'])
        print( target_crs )
        
        transform, width, height = calculate_default_transform(src.crs, target_crs, target_meta['width'], target_meta['height'], *src.bounds)
        
        kwargs = src.meta.copy()
        kwargs.update({
            'crs': target_crs,
            'transform': transform,
            'width': width,
            'height': height,
            'dtype': target_meta['dtype']
        })
        
        data = np.empty(shape=(height, width), dtype=kwargs['dtype'])

        reproject(
            source=rasterio.band(src, 1),
            destination=data,
            src_transform=src.transform,
            src_crs=src.crs,
            dst_transform=transform,
            dst_crs=target_crs,
            resampling=Resampling.nearest
        )

        return data, kwargs
#
#
#
def reproject_sp2_files(folder_path):
#reprojects sp2 files acroding to idela bounds for given area
    config = load_config()
    target_crs = get_ideal_utm_zone_epsg_for_bounds(config)
    print('target crs',target_crs)
    for file in os.listdir(folder_path):
        if file.endswith(config['snowpass_2_name']):
            file_path = os.path.join(folder_path, file)
            file_array = file_path.split('_')

            # Check if the CRS in the filename matches the target CRS
            if file_array[-3] != (target_crs.split(':')[1]):
                print('if loop',file_array[-3])
                print((target_crs.split(':')[1]))
                crs_without_the_ne_for_northern_hemisphere = (target_crs.split(':')[1])
                print('crs_without_the_ne_for_northern_hemisphere',crs_without_the_ne_for_northern_hemisphere)
                file_array[-3] = f"reproj_{crs_without_the_ne_for_northern_hemisphere}"
                new_file_path = '_'.join(file_array)
                
                with rasterio.open(file_path) as src:
                    # Use the resample_and_reproject function with the target CRS
                    target_meta = src.meta.copy()
                    data, kwargs = resample_and_reproject(file_path, target_meta, target_crs)

                    # Create output dataset with the reprojected data
                    with rasterio.open(new_file_path, 'w', **kwargs) as dst:
                        for i in range(1, src.count + 1):
                            reproject(
                                source=rasterio.band(src, i),
                                destination=rasterio.band(dst, i),
                                src_transform=src.transform,
                                src_crs=src.crs,
                                dst_transform=kwargs['transform'],
                                dst_crs=target_crs,
                                resampling=Resampling.nearest
                            )

                print(f"Reprojected file saved at: {file}")
            else:
                print(f"{file} is already in the correct projection.")
#
#
#
#
def check_and_resize_tifs(directory, desired_resolution):
    
    for filename in os.listdir(directory):
        if filename.endswith('.tif'):
            file_path = os.path.join(directory, filename)

            with rasterio.open(file_path) as src:
                # Get the current pixel size
                current_resolution = src.res

                # Check if the pixel size is 10x10 meters
                if (current_resolution[0] != desired_resolution[0]) or (current_resolution[1] != desired_resolution[1]):
                    print(f"Resampling {filename} from {current_resolution} to {desired_resolution}x{desired_resolution} m resolution.")
                    
                    # Calculate the new shape for the resampled image
                    scale_factor_x = current_resolution[0] / desired_resolution[0]
                    scale_factor_y = current_resolution[1] / desired_resolution[1]
                    new_width = int(src.width * scale_factor_x)
                    new_height = int(src.height * scale_factor_y)

                    # Resample the data
                    data = src.read(
                        out_shape=(src.count, new_height, new_width),
                        resampling=Resampling.bilinear
                    )

                    # Update metadata for the new file
                    new_meta = src.meta.copy()
                    new_meta.update({
                        'height': new_height,
                        'width': new_width,
                        'transform': src.transform * src.transform.scale(
                            (src.width / new_width),
                            (src.height / new_height)
                        )
                    })

                    # Overwrite the original TIFF file with the resampled data
                    with rasterio.open(file_path, 'w', **new_meta) as dst:
                        dst.write(data)

                    print(f"{filename} has been brought to uniform pixel size.")
                else:
                 print(f"{filename} is already at {desired_resolution}x{desired_resolution} m resolution.")




def delete_files_with_same_date_in_folder(parent_dir):
    # Iterate through all folders in the parent directory
    for item_name in os.listdir(parent_dir):
        item_path = os.path.join(parent_dir, item_name)

        if os.path.isfile(item_path):
            if (item_name.endswith('.csv') or
                item_name.endswith('.tiff')):
                os.remove(item_path)


        elif os.path.isdir(item_path):
            if item_name.endswith('_stats'):
                try:
                    shutil.rmtree(item_path)  # Removes non-empty directories
                    print(f"Deleted folder: {item_path}")
                except Exception as e:
                    print(f"Failed to delete folder {item_path}. Error: {e}")



        # Skip folders that don't start with a valid date
        if os.path.isdir(item_path) and item_name[:10].count('-') == 2:  # Check if folder name starts with a date
            # Extract the date part from the folder name (before the "T")
            folder_date = item_name.split('T')[0]  # Get the date (e.g., "2024-11-13")

            # Iterate through all files in the folder
            for file_name in os.listdir(item_path):
                # Check if the file starts with the same date as the folder
                if file_name.startswith(folder_date):
                    file_path = os.path.join(item_path, file_name)
                    os.remove(file_path)  # Delete the file
               