import rasterio
import geopandas as gpd
from rasterio.mask import mask
import numpy as np
import yaml
import functions
import os
from rasterio.windows import from_bounds
import time
from datetime import datetime, timedelta
import pandas as pd
import matplotlib.pyplot as plt
'''
This script calculates the percentage of pixes that are calssified with snow or no snow out of all pixels that are able to be classified (excluding forest pixels that dont get classified)
'''


def calc_snowline_stats(filtered_snow, modified_elevation, current_exposition, detailed_file_path):
    config = functions.load_config()
    threshold_value_low=config['lower_threshold_classification']
    threshold_value_high=config['upper_threshold_classification']

    #nodata vlaue for elevation files (higher than the highest point possible)
    nodata_value_elev = 9000
    #Creates elveation map for snow or no snow pixels
    filtered_elevation_all = np.where(np.logical_or(filtered_snow == 1, filtered_snow == 2), modified_elevation, nodata_value_elev)
    #Creates elevation map for only snow pixels
    filtered_elevation_snow = np.where(filtered_snow == 2, modified_elevation, nodata_value_elev)

    #writes all unique values down
    # but exludes everything above 8999 (9000 is used as elevation nodata value)
    unique_values = np.unique(filtered_elevation_all[filtered_elevation_all < 8999])


    #for exposition in cropped_aspect:
    percentages = {}

    for value in unique_values:
        # Create a mask where inputfile_mask equals the current value and input_snowpass1 snow or no snow but not nodata
        total_pixels_each_elev = np.sum(filtered_elevation_all == value)
    

    
    # Count pixels where both count_value and input_snowpass1 == 2 are true
        count_snow = np.sum(filtered_elevation_snow == value)
    
        if total_pixels_each_elev > 0:
            percentage = (count_snow / total_pixels_each_elev) * 100
        else:
            percentage = 0.0  # I case of division by zero 

        percentages[value] = percentage
    if config['detailed_analysis']: 
        update_file_detailed(detailed_file_path, current_exposition, percentages)


    sorted_percentages = sorted(percentages.items(), reverse=True)
    last_value_above_threshold_high = None
    last_value_above_threshold_low = None

    for value, percentage in sorted_percentages:
        if percentage > threshold_value_low:
            last_value_above_threshold_low = value
        else:
            break

    if last_value_above_threshold_low is not None:
        print(' ')
        print(f"The last Elevation on the {current_exposition}-Side above {threshold_value_low}% Snowcover is: {last_value_above_threshold_low}")
        print(' ')
    else:
        print(f"No elevation found above {threshold_value_low}% Snowcover")
        last_value_above_threshold_low=9000


    for value, percentage in sorted_percentages:
        if percentage > threshold_value_high:
            last_value_above_threshold_high = value
        else:
            break

    if last_value_above_threshold_high is not None:
        print(' ')
        print(f"The last Elevation on the {current_exposition}-Side above {threshold_value_high}% Snowcover is: {last_value_above_threshold_high}")
        print(' ')
    else:
        print(f"No elevation found above {threshold_value_high}% Snowcover")
        last_value_above_threshold_high=9000

    return last_value_above_threshold_low, last_value_above_threshold_high


#Functions to create and write the stats file 
#First structure of the file 
def initialize_file_detailed(file_path, altitudes, region, message):

    header = f"Snowline Statistics; :;{region};{message}\n"  
    column_titles = "Altitude;All Expositions;Exposition North;Exposition East;Exposition South;Exposition West\n" 

    with open(file_path, 'w') as f:
        f.write(header)  
        f.write(column_titles)  
        # Write initial altitudes with default values
        for altitude in altitudes:
            f.write(f"{altitude};-;-;-;-;-\n")  

#update the file for each exposition 
def update_file_detailed(file_path, exposition, percentages):
    
    temp_file = file_path + ".tmp"  # Temporary file for updates
    exposition_columns = {
        'All Expositions': 1,
        'North': 2,
        'East': 3,
        'South': 4,
        'West': 5
    }

    with open(file_path, 'r') as f, open(temp_file, 'w') as temp_f:
        lines = f.readlines()  # Read all lines from the original file
        
        # Write the first two header lines unchanged
        temp_f.write(lines[0]) 
        temp_f.write(lines[1]) 

        # Loop through the remaining lines and update based on the exposition
        for line in lines[2:]:  # Skip the first two lines (headers)
            parts = line.strip().split(';')  
            altitude = int(parts[0])  #the altitude in the file
            
            if altitude in percentages:  
                # Find the correct column for the current exposition
                column_index = exposition_columns.get(exposition)
                if column_index is not None:  # If the exposition is valid
                    parts[column_index] = f"{percentages[altitude]:.2f}%"  
            
    
            temp_f.write(';'.join(parts) + '\n')

    os.replace(temp_file, file_path)

def remove_empty_columns_detailed(file_path):
    temp_file = file_path + ".tmp"
    with open(file_path, 'r') as f, open(temp_file, 'w') as temp_f:
        lines = f.readlines()
        
        # Read the header
        title = lines[0]
        header = lines[1].strip().split(';')
        
        # Transpose the rows into columns for easier checking
        columns = list(zip(*[line.strip().split(';') for line in lines[2:]]))

        # Determine which columns are empty (excluding Altitude column)
        columns_to_keep = [0]  # Always keep the 'Altitude' column
        for i in range(1, len(columns)):
            # Check if the column contains anything other than placeholder ("-" or empty)
            if any(entry != '-' and entry != '0%' for entry in columns[i]):
                columns_to_keep.append(i)

        # Write the new header (excluding empty columns)
        new_header = [header[i] for i in columns_to_keep]
        temp_f.write(lines[0])
        temp_f.write(';'.join(new_header) + '\n')
        
        # Write the remaining lines (excluding empty columns)
        for line in lines[2:]:
            parts = line.strip().split(';')
            new_line = [parts[i] for i in columns_to_keep]
            temp_f.write(';'.join(new_line) + '\n')

    # Replace the original file with the cleaned file
    os.replace(temp_file, file_path)


#Functions to create and write the stats file 
#First structure of the file 
def initialize_file_overview(regions_list, merged_filepath):
    config = functions.load_config()
    filename=merged_filepath.replace(config['filename_prefix_for_merged_file_without_pixel_date_info'], "Statistics_").replace(".tiff", ".csv")

    lower_threshold = config['lower_threshold_classification']
    upper_threshold = config['upper_threshold_classification']

    last_date=merged_filepath.split('/')[-1].split('_')[-10]
    try: #amount_of_days_looked_back is a number 
        amount_of_days_looked_back = int(merged_filepath.split('/')[-1].split('_')[-13])
    except:  #amount_of_days_looked_back is string all
        amount_of_days_looked_back = str(merged_filepath.split('/')[-1].split('_')[-13])
    if amount_of_days_looked_back == 'all':
        line1=f"Data using all captured images starting from {config['start_date']} until {last_date}. New data overwrites old data. But new nodata like clouds does not!\n"
        amount_of_days_looked_back = 25
    else: 
        start_date = datetime.strptime(last_date, "%Y-%m-%d")- timedelta(days=amount_of_days_looked_back)
        start_date=start_date.strftime("%Y-%m-%d")
        line1=f"Data using all captured images between {start_date} until {last_date}. {amount_of_days_looked_back} days in total. New data overwrites old data. But new nodata like clouds does not!\n"

    


    line2= f"Lower threshold for Snow is last elevation above {lower_threshold}% of Snow and the upper threshold is the last elevation above {upper_threshold}% of Snow.\n"

    column_titles1 = ";All Expositions;All Expositions;North;North;East;East;South;South;West;West\n" 

    column_titles2 = f"Region;{lower_threshold}%;{upper_threshold}%;{lower_threshold}%;{upper_threshold}%;{lower_threshold}%;{upper_threshold}%;{lower_threshold}%;{upper_threshold}%;{lower_threshold}%;{upper_threshold}%\n" 


    with open(filename, 'w') as f:
        f.write(line1)  
        f.write(line2)
        f.write(column_titles1)  
        f.write(column_titles2)  
        for region in regions_list:
            f.write(f"{region};-;-;-;-;-;-;-;-;-;-\n")  



#update the file for each exposition 
def update_file_overview(merged_filepath, region, exposition, Snowline_low, Snowline_high):
    config = functions.load_config()

    # Construct the filename for the statistics CSV
    filename = merged_filepath.replace(config['filename_prefix_for_merged_file_without_pixel_date_info'], "Statistics_").replace(".tiff", ".csv")
    temp_file = filename + ".tmp"  # Temporary file for updates

    exposition_columns = {
        'All Expositions': 1,
        'North': 3,
        'East': 5,
        'South': 7,
        'West': 9
    }
    with open(filename, 'r') as f, open(temp_file, 'w') as temp_f:
        lines = f.readlines()  # Read all lines from the original file
         
        # Write the first four header lines unchanged
        temp_f.writelines(lines[:4])  

        # Loop through the remaining lines and update based on the exposition
        for line in lines[4:]:  # Skip the first 4 lines (headers)
            parts = line.strip().split(';')
            region_name = str(parts[0])  # Extract region name
            
            if region_name == region:  
                # Find the correct column for the current exposition
                column_index = exposition_columns.get(exposition)

                if column_index is not None:  # If the exposition is valid
                    # Update the corresponding column with Snowline values

                    parts[column_index] = f"{Snowline_low if Snowline_low != 9000 else '-'};{Snowline_high if Snowline_high != 9000 else '-'}"
                    #print(parts[column_index])
            
            # Write the updated or unchanged line to the temporary file
            temp_f.write(';'.join(parts) + '\n')

    # Replace the original file with the updated temporary file
    os.replace(temp_file, filename)

def remove_empty_columns_overview(merged_filepath, regions_out_of_bounds=None):
    if regions_out_of_bounds is None:
        regions_out_of_bounds = []
    
    config = functions.load_config()

    filename = merged_filepath.replace(
        config['filename_prefix_for_merged_file_without_pixel_date_info'], 
        "Statistics_"
    ).replace(".tiff", ".csv")

    temp_file = filename + ".tmp"
    with open(filename, 'r') as f, open(temp_file, 'w') as temp_f:
        lines = f.readlines()
        
        # Read the header
        title = lines[0:2]
        header = lines[2:4]
        header_parts = [line.strip().split(';') for line in header]

        # Transpose the rows into columns for easier checking
        columns = list(zip(*[line.strip().split(';') for line in lines[4:]]))

        # Determine which columns are empty (excluding Altitude column)
        columns_to_keep = [0]  # Always keep the 'Region' column
        for i in range(1, len(columns)):
            if any(entry != '-' for entry in columns[i]):  # Check for non-placeholder values
                columns_to_keep.append(i)

        # Write the title and updated header
        temp_f.writelines(title)
        new_header = [';'.join([header_line[i] for i in columns_to_keep]) for header_line in header_parts]
        temp_f.writelines([line + '\n' for line in new_header])

        # Process the remaining lines (filter empty columns and out-of-bounds regions)
        for line in lines[4:]:
            parts = line.strip().split(';')
            region_label = parts[0]  # Assume the first column contains the region label
            
            # Skip lines for regions that are out of bounds
            if region_label in regions_out_of_bounds:
                continue

            # Write the cleaned line (exclude empty columns)
            new_line = [parts[i] for i in columns_to_keep]
            temp_f.write(';'.join(new_line) + '\n')

        # Append information about removed regions
        if regions_out_of_bounds:
            temp_f.write("\nInfo: The following regions were out of bounds:\n")
            for region in regions_out_of_bounds:
                temp_f.write(f"{region}\n")

    # Replace the original file with the cleaned file
    os.replace(temp_file, filename)


def create_plots_detailed(detailed_file_path):
    output_png_path = detailed_file_path.replace(".csv", ".png")  
    region_name = os.path.basename(detailed_file_path).replace(".csv", "")
    region_name = region_name.replace("_stats","").replace("_"," ")
    with open(detailed_file_path, 'r') as file:
        lines = file.readlines()


    header_index = 1  
    data_index = 2   

    header = lines[header_index].strip().split(';')
    data = [line.strip().split(';') for line in lines[data_index:]]
    #print(data)
    # Create a DataFrame
    df = pd.DataFrame(data, columns=header)
    df = df.apply(
    lambda col: col.str.replace('%', '').replace('-', 'NaN').astype(float) if col.name != "Altitude" else col
    )
    df["Altitude"] = df["Altitude"].astype(float)
    
    # Plot the data
    plt.figure(figsize=(10, 6))
    
    for column in df.columns[1:]:  # Skip the Altitude column
        plt.plot(df["Altitude"], df[column], label=column)
    
    # Add labels, title, and legend
    plt.xlabel("Altitude (m)")
    plt.ylabel("Snow Cover Percentage (%)")
    plt.title(f"Snowline Statistics for: \n {region_name}")
    plt.legend()
    plt.grid(True)
    plt.ylim(0, 100)
    
    # Save the plot
    plt.savefig(output_png_path)
    plt.close()
    print(f"Plot saved to {output_png_path}")





def main(stats_run_as_standalone = False):
    config = functions.load_config()
    root_folder = config['download_and_processfolder']
    file_prefix = config['filename_prefix_for_merged_file_without_pixel_date_info']
    files = functions.list_files_with_prefix(root_folder, file_prefix)

    #values for snow no snow forest and cloud in the snow classification file
    snow_val = 2
    no_snow_val = 1
    forest_val = 3
    cloud_val = 4
 

    #if activated user interaction is necesary
    if config['ask_which_file_to_analyse'] or stats_run_as_standalone: 
        if functions.display_files(files):
            snow_raster_path = functions.choose_file(files)
            print(f"You have selected: {snow_raster_path}")
        else: 
            print('\n\nNo merged files found. Script stops. Create a merged File first!')
            time.sleep(5) 
            quit()
    #else latest date with reight settings of 'days to look back' will be choosen
    else:
        latest_date= None
        for file in files:
            amount_of_days_looked_back = file.split('/')[-1].split('_')[-13] #extracts how many days looked back info
            if amount_of_days_looked_back != config['number_of_days_to_look_back']:
                continue
            else:
                file_date=file.split('/')[-1].split('_')[-10] # extreacts date
                if latest_date == None or latest_date < file_date:
                    latest_date=file_date
                    snow_raster_path = file 

        if latest_date == None:
            print('\n\nNo valid merged file found. Script stops.\nYou can set ask_which_file_to_analyse to yes and choose the file on your own if there is one.')
            time.sleep(5) 
            quit()
        else: 
            print('\n\n')
            print('Following file is being used for analysis of the snowline:', snow_raster_path)
            print('\n\n')  

    # Loading shp files
    regions_path = config['regions_vectorfile_path']
    project_epsg=functions.get_ideal_utm_zone_epsg_for_bounds(config).split(':')[1]
    #reproject shp file 
    #microregions = microregions.to_crs(epsg=project_epsg)
    #debugging view the first few rows of the data (including geometries)
    #print(microregions.head())
    #print(microregions.dtypes)

    #construct filename of elevation and aspect file
    masks_path=os.path.join(config['static_path'],config['processed_masks'])
    stepsize = config['elev_stepsize']
    elev_steps_raster = f'{functions.filename_from_bounds(config)}_DEM_elev_{stepsize}_m_steps.tif'
    elev_path = os.path.join(masks_path,elev_steps_raster)
    #print(elev_path)
    aspect_raster =f"{functions.filename_from_bounds(config)}_DEM_aspect.tif"
    aspect_path = os.path.join(masks_path,aspect_raster)
   

    microregions = gpd.read_file(regions_path)


    #Following part names the regions of the shp file that can be only id only name or both combined. If nothing is defined in config (shp_file_id, shp_file_name) the code asks if user is fine with numbering them from randomly (not a good idea to identify the diferent regions)
    id_column = config['vector_file_id']
    name_column = config['vector_file_name']

    #Naming the entries for each region
    # Check if 'id' and 'name' exist in the columns
    if name_column in microregions.columns and id_column in microregions.columns:
        # Use both 'id' and 'name' 
        microregions['area_label'] = microregions[id_column].astype(str) + "_" + microregions[name_column].astype(str)
        print("Using both 'id' and 'name' for area labels.")
        
    elif id_column in microregions.columns and name_column not in microregions.columns:
        # Use only 'id' 
        microregions['area_label'] = microregions[id_column].astype(str)
        print("Using 'id' only for area labels as 'name' doesn't exist.")

    elif id_column not in microregions.columns and name_column in microregions.columns:
        # Use only 'name' 
        microregions['area_label'] = microregions[name_column].astype(str)
        print("Using 'name' only for area labels as 'id' doesn't exist.")
         
    elif id_column not in microregions.columns:
        # If 'id' does not exist, show a warning and ask the user
        print("Warning: 'id' column not found!")
        response = input("No 'id' found. Do you want to continue? IDs will be added starting from 1. (yes/no): ").strip().lower()
        
        if response == 'yes' or response == 'y':
            # If the user agrees, create an 'id' column starting from 1
            microregions[id_column] = range(1, len(microregions) + 1)
            microregions['area_label'] = microregions[id_column].astype(str)
            print("Added 'id' column starting from 1.")
        else:
            print("Operation cancelled.")
            quit()


    #detailed analysis creates an extra folder and file with stats for each region and choosen aspect for each elevation level there is three if config['detailed_analysis']: blocks in this script
    if config['detailed_analysis']:
        detailed_analysis_path= f'{snow_raster_path.split(".")[0]}_stats'
        if not os.path.exists(detailed_analysis_path):
            os.makedirs(detailed_analysis_path)

    initialize_file_overview(microregions['area_label'], snow_raster_path)
 


    project_epsg=functions.get_ideal_utm_zone_epsg_for_bounds(config).split(':')[1]
    if microregions.crs != project_epsg:
        # Reproject the microregions to the correct UTM CRS
        microregions = microregions.to_crs(epsg=project_epsg)
        print(f"Reprojected microregions to EPSG:{project_epsg}")

    else:
        print(f"Microregions already in EPSG:{project_epsg}") 


    #creates cutouts of each region for the aspect elevation and snowpass file to check for problems
    if config['debugging_comments']:
        output_dir=config['download_and_processfolder']+'/microregions_cutouts_debugging/'
        if not os.path.exists(output_dir):
            os.mkdir(output_dir) 
    regions_out_of_bounds = None
    #go through regions
    for index, region in microregions.iterrows():
        if config['debugging_comments']:
            print('Now processing:',region)
        geom = [region['geometry']]
        #check if geom is in bounds
        geom_in_bounds=functions.is_geometry_in_bounds(geom)
        if not geom_in_bounds: 
            print('Regin ', region['area_label'],'is not in Bounds! Continuing with next region!\n') 

            if not regions_out_of_bounds:
                regions_out_of_bounds = []

            regions_out_of_bounds.append(region['area_label'])
            #print(regions_out_of_bounds)
            continue
        if geom_in_bounds:

            with rasterio.open(snow_raster_path) as src_snow:
                crs=src_snow.crs
                snow_class_region, out_transform = mask(src_snow, geom, crop=True)
                meta_cropped_snow = src_snow.meta.copy()
                meta_cropped_snow.update({
                    "driver": "GTiff",
                    "height": snow_class_region.shape[1],
                    "width": snow_class_region.shape[2],
                    "transform": out_transform
                })
                pixel_width, pixel_height = src_snow.res


                if config['debugging_comments']:
                    print('Snowpass_2 resolution:',src_snow.res)
               
                    # Save clipped raster for debugging
                    out_filename = os.path.join(output_dir, f'{region["area_label"]}_{index}_Snowpass_clipped.tif')
                    with rasterio.open(out_filename, 'w', **meta_cropped_snow) as dest:
                        dest.write(snow_class_region)
            
            with rasterio.open(elev_path) as src_elev:
                elev_region, out_transform = mask(src_elev, geom, crop=True)
                meta_cropped_elev = src_elev.meta.copy()
                meta_cropped_elev.update({
                    "driver": "GTiff",
                    "height": elev_region.shape[1],
                    "width": elev_region.shape[2],
                    "transform": out_transform
                })
                if config['debugging_comments']:
                    print('Elevation resolution:',src_elev.res)
                    out_filename = os.path.join(output_dir, f'{region["area_label"]}_{index}_elev_clipped.tif')
                    with rasterio.open(out_filename, 'w', **meta_cropped_elev) as dest:
                        dest.write(elev_region)

            with rasterio.open(aspect_path) as src_aspect:
                aspect_region, out_transform = mask(src_aspect, geom, crop=True)
                meta_cropped_aspect = src_aspect.meta.copy()
                meta_cropped_aspect.update({
                    "driver": "GTiff",
                    "height": aspect_region.shape[1],
                    "width": aspect_region.shape[2],
                    "transform": out_transform
                })
                if config['debugging_comments']:
                    print('Elevation resolution:',src_aspect.res)
                    out_filename = os.path.join(output_dir, f'{region["area_label"]}_{index}_aspect_clipped.tif')
                    with rasterio.open(out_filename, 'w', **meta_cropped_aspect) as dest:
                        dest.write(aspect_region)



            if len({snow_class_region.shape[1:], elev_region.shape[1:], aspect_region.shape[1:]}) != 1:
                height = min(snow_class_region.shape[1], elev_region.shape[1], aspect_region.shape[1])
                width = min(snow_class_region.shape[2], elev_region.shape[2], aspect_region.shape[2])
                    # Update snow_class_region for the output files
                meta_cropped_snow.update({
                    'crs': crs,
                    'height': height,
                    'width': width
                })
                
                meta_cropped_elev.update({
                    'crs': crs,
                    'height': height,
                    'width': width
                })

                meta_cropped_aspect.update({
                    'crs': crs,
                    'height': height,
                    'width': width
                })
               
                
            # Resample data to the same dimensions
            snow_class_region = snow_class_region[:, :height, :width]
            elev_region = elev_region[:, :height, :width]
            aspect_region = aspect_region[:, :height, :width]

            # Set pixels to 1 if they equal snow_val or no_snow_val, otherwise set to 0
            valid_pixels_mask = np.where((snow_class_region == snow_val) | (snow_class_region == no_snow_val), 1, 0)

            # Count the number of valid pixels (snow or no snow)
            num_valid_pixels = valid_pixels_mask.sum()

            if config['debugging_comments']:
                    out_filename = os.path.join(output_dir, f'{region["area_label"]}_{index}_valid_pixels_mask.tif')
                    with rasterio.open(out_filename, 'w', **meta_cropped_snow) as dest:
                        dest.write(valid_pixels_mask)

            nodata_value = 0
            #cropped_snow is only snow or no snow now 
            cropped_snow = np.where(valid_pixels_mask, snow_class_region, nodata_value)
          
            
            forest_pixels = np.sum(snow_class_region == forest_val)
            nodata_pixels = np.sum(snow_class_region == 0)
            snow_pixels = np.sum(snow_class_region == 2)
            no_snow_pixels = np.sum(snow_class_region == 1)
            cloud_pixels = np.sum(snow_class_region == 4)
            snow_no_snow = snow_pixels + no_snow_pixels
            total_pixels =  np.sum(forest_pixels+snow_pixels+no_snow_pixels+cloud_pixels)

            total_pixels_excl_forrest = total_pixels-forest_pixels

            # Calculate the percentage of valid pixels
            percentage_valid_pixels_forest = (num_valid_pixels / total_pixels_excl_forrest) * 100
            #percentage_valid_pixels_all = (num_valid_pixels / total_pixels) * 100 this value makes no sense beacuse forest cant be classified in this algorithm
     
            #''' 
            if config['debugging_comments']:
                print('num valid pixeles', num_valid_pixels)
                print('snow Pixels', no_snow_pixels)
                print('snow Pixels', snow_pixels)
                print('cloud pixels', cloud_pixels) #debugger tested with qgis image
                print('total Pixels', total_pixels)  
                print('nodata pixels',nodata_pixels) #debugger tested with qgis image
                print('forest Pixels', forest_pixels) #debugger tested with qgis image
                print('percentage_valid_pixels_forest', percentage_valid_pixels_forest)

            message=f"Percentage of classified pixels in open areas (excluding forests) in {region['area_label']}: {percentage_valid_pixels_forest:.2f}%"

            if percentage_valid_pixels_forest < config['threshold_percentage_of_classified_pixels_to_continue_with_stats']:
                message = (f"Warning very little Data!  Only {percentage_valid_pixels_forest:.2f}% of open areas (without forest) classified. threshold for statistics is set to {config['threshold_percentage_of_classified_pixels_to_continue_with_stats']:.0f}% ")
                update_file_overview(snow_raster_path, region['area_label'], exposition ='All Expositions', Snowline_low='Not enough data!', Snowline_high=f'Only {percentage_valid_pixels_forest:.2f}% of open areas classified. threshold for statistics is set to {config["threshold_percentage_of_classified_pixels_to_continue_with_stats"]:.0f}%')
                print(message)
                if config['detailed_analysis']:
                    unique_altitudes = np.unique(elev_region[elev_region < 8999])
                    detailed_file_path=os.path.join(detailed_analysis_path,f"{region['area_label']}_stats.csv")
                    initialize_file_detailed(detailed_file_path, unique_altitudes,region['area_label'],message)
                continue

            # Ensure that the shapes of the masks match before applying the mask
            if valid_pixels_mask.shape == elev_region.shape:
                # Apply the valid_pixels_mask to the masked_elev
                nodata_value_elev=9000
                filtered_elevation = np.where(valid_pixels_mask, elev_region, nodata_value_elev)

            else:
                print(f"\n\nShape mismatch: valid_pixels_mask shape is {valid_pixels_mask.shape}, masked_elev shape is {elev_region.shape} ")
                quit()

            if config['detailed_analysis']:
                unique_altitudes = np.unique(filtered_elevation[filtered_elevation < 8999])
                detailed_file_path=os.path.join(detailed_analysis_path,f"{region['area_label']}_stats.csv")
                initialize_file_detailed(detailed_file_path, unique_altitudes,region['area_label'],message)

            #diferent options to choose in config No expositions, only north and south or all expositions
            if config['expositions_to_look_at'] not in [0,1,2]:
                print('Check Config file: expositions_to_look_at has wrong value it should be 0,1 or 2 ')
                quit()

            if config['expositions_to_look_at'] == 0: 
                current_exposition = 'All Expositions'
                Snowline_low, Snowline_high =calc_snowline_stats(snow_class_region, filtered_elevation, current_exposition, detailed_file_path)
                update_file_overview(snow_raster_path, region['area_label'], current_exposition, Snowline_low, Snowline_high)

            if config['expositions_to_look_at'] == 1: 
                current_exposition = 'North'
                filtered_elevation_north = np.where(aspect_region ==1, elev_region, nodata_value_elev)
                Snowline_low, Snowline_high =calc_snowline_stats(snow_class_region, filtered_elevation_north, current_exposition, detailed_file_path)
                update_file_overview(snow_raster_path, region['area_label'], current_exposition, Snowline_low, Snowline_high)

                current_exposition = 'South'
                filtered_elevation_north = np.where(aspect_region ==3, elev_region, nodata_value_elev)
                Snowline_low, Snowline_high =calc_snowline_stats(snow_class_region, filtered_elevation_north, current_exposition, detailed_file_path)
                update_file_overview(snow_raster_path, region['area_label'], current_exposition, Snowline_low, Snowline_high)



            if config['expositions_to_look_at'] == 2: 
                current_exposition = 'North'
                filtered_elevation_north = np.where(aspect_region ==1, elev_region, nodata_value_elev)
                Snowline_low, Snowline_high =calc_snowline_stats(snow_class_region, filtered_elevation_north, current_exposition, detailed_file_path)
                update_file_overview(snow_raster_path, region['area_label'], current_exposition, Snowline_low, Snowline_high)


                current_exposition = 'East'
                filtered_elevation_east = np.where(aspect_region == 2, elev_region, nodata_value_elev)
                Snowline_low, Snowline_high  = calc_snowline_stats(snow_class_region, filtered_elevation_east, current_exposition, detailed_file_path)
                update_file_overview(snow_raster_path, region['area_label'], current_exposition, Snowline_low, Snowline_high)


                current_exposition = 'South'
                filtered_elevation_north = np.where(aspect_region ==3, elev_region, nodata_value_elev)
                Snowline_low, Snowline_high =calc_snowline_stats(snow_class_region, filtered_elevation_north, current_exposition, detailed_file_path)
                update_file_overview(snow_raster_path, region['area_label'], current_exposition, Snowline_low, Snowline_high)


                current_exposition = 'West'
                filtered_elevation_west = np.where(aspect_region == 4, elev_region, nodata_value_elev)
                Snowline_low, Snowline_high = calc_snowline_stats(snow_class_region, filtered_elevation_west, current_exposition, detailed_file_path)
                update_file_overview(snow_raster_path, region['area_label'], current_exposition, Snowline_low, Snowline_high)

        if config['detailed_analysis']:
             remove_empty_columns_detailed(detailed_file_path)
             create_plots_detailed(detailed_file_path)

    remove_empty_columns_overview(snow_raster_path, regions_out_of_bounds)
        
        



if __name__ == "__main__":
    functions.remove_dot_config()
    main(stats_run_as_standalone = True)