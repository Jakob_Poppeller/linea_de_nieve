import rasterio
import numpy as np
import functions
import yaml
import os
import sys
from datetime import datetime

def main(stats_run_as_standalone = False):
    config = functions.load_config()

    #user interaction how many days do you want to look back
    user_input= config['number_of_days_to_look_back']
    while True:
        if user_input is None:
            user_input = input("How many days do you want to look back (between 1 and 24, or type 'all'): ").lower()
        if user_input == 'all':
            day_range = None  # No restriction on pixel values, use all data
            filename_day_range=config['filename_prefix_for_merged_file_without_pixel_date_info']+'_all_days_before' 
            break
        try:
            days = int(user_input)
            if 1 <= days <= 24:
                filename_day_range=config['filename_prefix_for_merged_file_without_pixel_date_info']+'_last_'+str(days)+'_days_before'  
                day_range = days * 10 + 9  #Example for 10 days -> use values between 0 and 109
                break
            else:
                print("Please enter a number between 1 and 24.")
                user_input = input("How many days do you want to look back (between 1 and 24, or type 'all'): ").lower()
        except ValueError:
            print("Invalid input. Please enter a number between 1 and 24 or 'all'.")
            user_input = input("How many days do you want to look back (between 1 and 24, or type 'all'): ").lower()


    # Load configuration
    epsg = (functions.get_ideal_utm_zone_epsg_for_bounds(config).split(':')[1])
    bounds_name = functions.filename_from_bounds(config)
    folder=config['download_and_processfolder']
    files_processed = False
    if not os.path.isdir(folder):
        print(f"Error: Directory '{folder}' does not exist.")
        sys.exit(1)  # Exit the script with an error code
    latest_date = None # Set latest date to none before the loop starts
    # Traverse through the directory to find matching files
    for root, dirs, files in os.walk(folder):
        #print("Current Directory:", root)
        #print("Subdirectories:", dirs)
        #print("Files:", files)
        #print()
        for file in files:
            if file.endswith(f"{epsg}_{bounds_name}_merged_snowpass_2.tiff"): 
                file_splited=file.split('_')
                if file_splited[0].startswith(config['filename_prefix_for_merged_file_without_pixel_date_info'].split('_')[0]):
                    continue

                file_date = datetime.strptime(file_splited[0], "%Y-%m-%d")
        
                # Compare dates to find the most recent one (if you dont remove older files -> config save_files_from_last_run)
                if latest_date is None or latest_date < file_date:
                    latest_date = file_date
                    latest_file = file
                    
                    file_path = os.path.join(root, latest_file)

                    # Open the raster image
                    with rasterio.open(file_path) as src:
                        # Read the image data into a NumPy array
                        data = src.read(1)  # Assuming it's a single-band image
                        
                        # Modify the array by keeping only the last digit of each pixel value
                        modified_data = np.abs(data) % 10  # Use modulus to get the last digit
                        # If a specific day range is provided, filter the pixel values

                        if day_range is not None:
                            mask = data <= day_range
                            modified_data = np.where(mask, modified_data, 0)  # Keep values within range, else set to 0
                        
                        # Use the profile from the original image to save the modified image
                        profile = src.profile
                        profile.update(dtype=rasterio.uint8, nodata=0)  # Set nodata to 0

                        # Use the profile from the original image to save the modified image
                        profile = src.profile
                        profile.update(dtype=rasterio.uint8, nodata=0)  # Set nodata to 0

                    # Construct the output file path
                    file_splited.insert(0, filename_day_range)
                    file = '_'.join(file_splited)
                    output_file = os.path.join(root, file)

                    # Write the modified data to a new raster file
                    with rasterio.open(output_file, 'w', **profile) as dst:
                        dst.write(modified_data, 1)
                   

                    print(f"Processed and saved: {output_file}")
                    files_processed = True

    if not files_processed:
        print("No Merged Snowpass files found to process!")

if __name__ == "__main__":
    functions.remove_dot_config()
    main(stats_run_as_standalone = True)
