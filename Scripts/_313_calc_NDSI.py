import rasterio
import numpy as np
import os
from rasterio.warp import calculate_default_transform, reproject, Resampling
import functions 


def main(folder_path):
    green_b3 = os.path.join(folder_path, 'B03.tiff')
    swir_b11 = os.path.join(folder_path, 'B11.tiff')
    output_file_name = os.path.join(folder_path, 'NDSI.tiff')
    # Open green band
    
    with rasterio.open(green_b3) as green_src:
        green = green_src.read(1)  
        meta_green = green_src.meta.copy()

    # Open SWIR band
    
    with rasterio.open(swir_b11) as swir_src:
        swir = swir_src.read(1)  

    if green.shape != swir.shape:
            #logging.info(f"Resampling SWIR band {swir_b11} to match the dimensions of green band {green_b3}")
        swir, kwargs = functions.resample_and_reproject(swir_b11, meta_green)
            
    # Calculate NDSI
    ndsi = (green.astype(float) - swir.astype(float)) / (green + swir).astype(float)

    # Handle division by zero (set NDSI to 0 where denominator is 0)
    ndsi[np.isnan(ndsi)] = 0
    ndsi[np.isinf(ndsi)] = 0
    # Update metadata for the output file
    kwargs.update(dtype=rasterio.float32, count=1)

    # Write the NDSI to a new file
    with rasterio.open(output_file_name, 'w', **kwargs) as dst:
        dst.write(ndsi.astype(rasterio.float32), 1)

    print(f"NDSI calculation complete. Output saved to {output_file_name}")

if __name__ == "__main__":
    functions.remove_dot_config()
    main(folder_path)
