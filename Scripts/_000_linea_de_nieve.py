# This script runs all scripts at once. 
# the Codes specify the diferent parts of the code:
# 1xx: download files
# 2xx: preprocess files
# 3xx: Classification process
# 4xx: merging Process
# 5xx: statistics

import functions
import yaml
import os
import _100_run_downloads
import _200_preprocessing
import _300_go_through_subfolders_and_classify_images
import _401_merge_sp2
import _402_cut_date_info_from_merged
import _501_get_microregion_stats
import _001_check_for_config_changes


def main():
    _001_check_for_config_changes.main()
    _100_run_downloads.main()
    _200_preprocessing.main()
    _300_go_through_subfolders_and_classify_images.main()
    _401_merge_sp2.main()
    _402_cut_date_info_from_merged.main()
    _501_get_microregion_stats.main()


if __name__ == "__main__":
    main()

