import os
import re
import shutil
import numpy as np
import rasterio
from rasterio.merge import merge
from rasterio.warp import calculate_default_transform, reproject, Resampling
from rasterio.transform import from_origin
from pyproj import Proj, transform
import yaml

import functions



def initialize_merged_data(allready_merged_sp2_file, config, resolution=10):
    
    if allready_merged_sp2_file is not None:
        with rasterio.open(allready_merged_sp2_file, 'r') as src:
            data = src.read(1)
            profile = src.profile
        return data, profile
    else:
        epsg= functions.get_ideal_utm_zone_epsg_for_bounds(config)
        # Define the bounding box and transform based on the resolution and EPSG
        bounds = functions.get_max_min_bounds(config)
             
        min_lon, min_lat, max_lon, max_lat = bounds

        # Create a projection for the specified EPSG
        proj = Proj(epsg)

        # Convert latitude and longitude to UTM coordinates
        minx, miny = proj(min_lon, min_lat)
        maxx, maxy = proj(max_lon, max_lat)
        #print(minx)
        #print(maxx)
        # Calculate width and height based on resolution, rounding up to ensure full coverage
        width = int(np.ceil((maxx - minx) / resolution))
        height = int(np.ceil((maxy - miny) / resolution))
        #print(width)
        # Create an array filled with zeros
        data = np.zeros((height, width), dtype='uint8')  # or 'float32' if needed

        # Create a transform for the raster
        transform = from_origin(minx, maxy, resolution, resolution)

        # Define the metadata for the raster
        profile = {
            'driver': 'GTiff',
            'dtype': 'uint8', 
            'count': 1,
            'width': width,
            'height': height,
            'crs': epsg,
            'transform': transform,
            'nodata': 255  # Define the NoData value
        }

        return data, profile


def resample_image_like_merged(image_path, reference_profile, reference_array):
    with rasterio.open(image_path) as src:
        src_array = src.read(1)

        # Check if the source image has the same shape and transform as the reference
        if src_array.shape == reference_array.shape and src.transform == reference_profile['transform']:
            # The image already matches the reference, so no resampling needed
            return src_array
 
        # Create an empty array with the same shape as the reference
        dst_array = np.empty_like(reference_array)

        if dst_array.shape[0] == 0 or dst_array.shape[1] == 0:
            raise ValueError("Invalid destination array dimensions, cannot proceed with reprojection.")

        # Reproject and resample the source image to match the reference profile
        reproject(
            source=src_array,
            destination=dst_array,
            src_transform=src.transform,
            src_crs=src.crs,
            dst_transform=reference_profile['transform'],
            dst_crs=reference_profile['crs'],
            resampling=Resampling.nearest,  
            dst_nodata=255  
        )
        
        return dst_array

def find_new_snowpass_files(directory, config):
    all_snowpass_files = []
    new_snowpass_files =[]
    merged_sp2_file = None
    epsg = (functions.get_ideal_utm_zone_epsg_for_bounds(config).split(':')[1])
    bounds_name= functions.filename_from_bounds(config)
    for root, dirs, files in os.walk(directory):
        #print("Current Directory:", root)
        #print("Subdirectories:", dirs)
        #print("Files:", files)
        #print()

        for file in files:
            #crates list of sp2 files
            if file.endswith(f"{epsg}_{config['snowpass_2_name']}"): 
                all_snowpass_files.append(os.path.join(root, file))

            #next if statement looks for latest merged sp2 file
            filename_pattern = re.compile(r"^\d{4}-\d{2}-\d{2}"+f"_EPSG_{epsg}_{bounds_name}_merged_snowpass_2\.tiff$")
            if filename_pattern.match(file): 
                if merged_sp2_file is None or file > merged_sp2_file: 
                    merged_sp2_file=os.path.join(root,file)

    latest_merge_date = None 
    if merged_sp2_file is not None:
        latest_merge_date = merged_sp2_file.split('_')[0]
    #print(merged_sp2_file)
    for sp_path in all_snowpass_files: 
        filename=sp_path.split('/')[-1] 
        date=filename.split('T')[0]
        if latest_merge_date is None or  latest_merge_date <= date:
            new_snowpass_files.append(sp_path)
    if config['debugging_comments']:
        print('New snowpass files:', new_snowpass_files)
        print('all  snowpass files:', all_snowpass_files)
    return new_snowpass_files, merged_sp2_file

def merge_tiffs(allready_merged_sp2_file, files, config):

    merged_data, profile = initialize_merged_data(allready_merged_sp2_file, config) #if merged data is None creates new file
    merged_data[merged_data == np.nan] = 0  # Sets all nan to 0

    data_type= 'uint8'
    '''
    include this and add parts (not working yet) to have more than 25 day to look back

    if config['number_of_dates_to_look_back_on'] > 25:
        data_type= 'uint8'
    else:
        data_type= 'uint16'
    '''
    
    #get latest date as a string from filenames
    newest_filename=files[-1].split('/')[-1]
    newest_date=newest_filename.split('T')[0]
    print('Newest date',newest_date)

    #print(allready_merged_sp2_file)

    #changes the date index to the value of the new merged file 
    if allready_merged_sp2_file is not None:
        filename=allready_merged_sp2_file.split('/')[-1] 
        last_merge_date=filename.split('_')[0]
        print('last merge date:',last_merge_date)
        pixel_value_raiser = functions.dates_in_between(last_merge_date, newest_date)*10
        print('Pixel value raiser for old merged data:', pixel_value_raiser)
          
        # if merged_data exeeds 255 after raising write 25+value else rise value
        modified_data = np.where(merged_data + pixel_value_raiser > 255, 25 + (merged_data % 10), merged_data + pixel_value_raiser)
        merged_data = modified_data
        

    for file in files:
        if config['debugging_comments']:
            print('at file:',file)
        date = file.split('/')[-1].split('T')[0]
        if allready_merged_sp2_file is None or date >= last_merge_date:
            try:
                with rasterio.open(file, 'r') as src:
                    date = file.split('/')[-1].split('T')[0]
                    if config['debugging_comments']:
                        print(date)
                    dates_between_raiser = functions.dates_in_between(newest_date, date)*10
                    if config['debugging_comments']:
                        print(dates_between_raiser)
                    data = src.read(1)
                    # Ensure empty pixels are set to 0
                    data[data == np.nan] = 0
                    modified_data = np.where(data + dates_between_raiser > 255, 25 + (data % 10), data + dates_between_raiser)
                    # Create masks for different conditions
                    ends_with_0 = (modified_data % 10 == 0) #nodata
                    ends_with_1_2_3 = np.isin(modified_data % 10, [1, 2, 3]) #snow or no snow or forest (no snow and forest)
                    ends_with_3 = (modified_data % 10 == 3) #forest
                    ends_with_4 = (modified_data % 10 == 4) #cloud
                    # Overwrite pixels in merged_data that end with 1, 2, 3
                    merged_data = np.where(ends_with_1_2_3, modified_data, merged_data)

                    # Overwrite empty pixels in merged_data with clouds (dont overwrite other things with clouds)
                    merged_empty_pixels = (merged_data % 10 == 0)  # Empty pixels have a 0 as last digit
                    merged_data = np.where(ends_with_4 & merged_empty_pixels, modified_data, merged_data)

                    # Overwrite pixels in merged_data with clouds (endswith4) if they are not 1,2,3 allready (more valuable data)
                    merged_data = np.where(ends_with_4 & ~np.isin(merged_data % 10, [1, 2, 3]), modified_data, merged_data)
                    #if you dont want forest in all AOI uncomment this and remove next part wich says print forest in AOI
                    #merged_data= np.where(ends_with_3,3,merged_data)

            except Exception as e:
                print(f"Error modifying file {file}: {e}")
                continue
        else: 
            print('File ',file, 'skipped is allready in Merged Data!\n')
            continue
    #print forest in all AOI
    forest_mask = os.path.join(config['static_path'],config['processed_masks'],f"{functions.filename_from_bounds(config)}_forest.tif")
    #with rasterio.open(forest_mask, 'r') as src:
    print('adding forest on merged area')
    
    forest_data = resample_image_like_merged(forest_mask, profile, merged_data)
    # Overwrite pixels in merged_data that are not classified as snow (cloud nodata forest and no snow) with forest
    merged_data = np.where((forest_data == 1) & (merged_data % 10 != 2), 3, merged_data)



    return merged_data, profile


    
def save_resampled_image(output_path, resampled_array, reference_profile):
    profile = reference_profile.copy()
    profile.update(dtype=rasterio.uint8, count=1)
    
    with rasterio.open(output_path, 'w', **profile) as dst:
        dst.write(resampled_array, 1)


def main():
    config = functions.load_config()
    directory=config['download_and_processfolder']
    epsg = (functions.get_ideal_utm_zone_epsg_for_bounds(config).split(':')[1])
    new_snowpass_files, allready_merged_sp2_file = find_new_snowpass_files(directory, config)
    merged_data, profile = initialize_merged_data(allready_merged_sp2_file, config)
    #resizing images to match original
    #print('new sp files',new_snowpass_files)
    for image_path in new_snowpass_files:
        resampled_image = resample_image_like_merged(image_path, profile, merged_data)
        save_resampled_image(image_path, resampled_image, profile)

    latest_date = functions.extract_latest_date(new_snowpass_files)
    bounds_name= functions.filename_from_bounds(config)
    new_fileneame=f'{latest_date}_EPSG_{epsg}_{bounds_name}_merged_snowpass_2.tiff'
    output_path = os.path.join(directory,new_fileneame)

    if not new_snowpass_files:
        print("No new Snowpass2 files found.")
        return
    new_snowpass_files.sort()
    if allready_merged_sp2_file is None:
        print('Creating new merged Snowpass File.')
    else: 
        print('Adding New Snowpass2 files to the old merged file.')

    #print("Sorted Snowpass2 files:", new_snowpass_files)

    try:
        merged_data, profile = merge_tiffs(allready_merged_sp2_file, new_snowpass_files,config)
        #print(profile)
        # Write merged data to an output file
        with rasterio.open(output_path, 'w', **profile) as dst:
            dst.write(merged_data, 1)  # Write the merged data

        print(f"Merged file saved at: {output_path}")
        if config['save_files_from_last_run'] is False:
            for file in os.listdir(directory):            
                if file.endswith(f"{epsg}_{bounds_name}_merged_snowpass_2.tiff") and not file.startswith(config['filename_prefix_for_merged_file_without_pixel_date_info']) and not str(file) == str(new_fileneame):
                        os.remove(os.path.join(directory,file))
                        print('Old file:',file,'deleted!')

        if config['delete_downloaded_folders_after_merging'] is True:
            pattern = re.compile(r"^\d{4}-\d{2}-\d{2}T\d{2}-\d{2}-\d{2}")
            with open(os.path.join(config['download_and_processfolder'],config['info_file']), 'a') as file:
                # Search for folders that match the pattern and delete them
                for folder in os.listdir(directory):
                    folder_path = os.path.join(directory, folder)
                    if os.path.isdir(folder_path) and pattern.match(folder):
                        file.write(folder + '\n')
                        shutil.rmtree(folder_path)
                        print(f"Deleted folder: {folder}")



    except Exception as e:
        print(f"Error merging Snowpass2 files: {e}")

if __name__ == "__main__":
    functions.remove_dot_config()
    main()