import requests
import json
import rasterio
import rasterio.merge
import affine
import os
import shutil
from zipfile import ZipFile
import yaml
import functions


#logs into copernicus 
def authorize(username, password):
    return json.loads(requests.post(
        "https://identity.dataspace.copernicus.eu/auth/realms/CDSE/protocol/openid-connect/token",
        headers = {
            "content-type": "application/x-www-form-urlencoded"
        },
        data = {
            "client_id": "cdse-public",
            "username": username,
            "password": password,
            "grant_type": "password"
        }
    ).content)["access_token"]


#constructs downlaod URL
def get_products(start_date, end_date, polygon, cloud_cover):
    url = f"https://catalogue.dataspace.copernicus.eu/odata/v1/Products?$filter=Collection/Name eq 'SENTINEL-2' and contains(Name,'MSIL2A') and OData.CSC.Intersects(area=geography'SRID=4326;POLYGON(({', '.join([f'{i[0]} {i[1]}' for i in polygon])}))') and Attributes/OData.CSC.DoubleAttribute/any(att:att/Name eq 'cloudCover' and att/OData.CSC.DoubleAttribute/Value le {cloud_cover}) and ContentDate/Start gt {start_date}T00:00:00.000Z and ContentDate/Start lt {end_date}T00:00:00.000Z&$orderby=ContentDate/Start desc"
    return json.loads(requests.get(url).content)

# Downlaods requested products 
def download_product(token, product_id, product_name, content_length, try_again = True):
    resp = requests.get(f"https://download.dataspace.copernicus.eu/odata/v1/Products({product_id})/$value", headers = {"Authorization": f"Bearer {token}"}, stream = True)
    with open(f"{product_name}.zip", "wb") as file:
        i = 0
        chunk_size = 8192
        for chunk in resp.iter_content(chunk_size = chunk_size):
            if chunk:
                file.write(chunk)
                i += 1
                if i % 100 == 0:
                    #prints the percentage of the curent downlaod
                    print(f"{(i * chunk_size / content_length * 100):.2f}% ({i * chunk_size} / {content_length})", end = "\r")
        print(f"100.00% ({content_length} / {content_length})\nDownload complete")
    try:
        with ZipFile(f"{product_name}.zip", "r") as f:
            f.extractall(".")
        os.remove(f"{product_name}.zip")
        return 
    except Exception as e:  
        print(f"Couldn't download or extract {product_name}: {e}")

        #retrys the downlaod one time
        if try_again:
            print("Trying again")
            download_product(token, product_id, product_name, content_length, try_again=False)
        else:
            
            print("Download failed.")
            try:
                os.remove(f"{product_name}.zip")  # Attempt to remove the file if it exists
            except OSError:
                print(f"Failed to delete {product_name}.zip (it might not exist).")
            raise  # Re-raise the original exception after cleanup

def is_folder_in_list(folder_name, list_file):
    if not os.path.exists(list_file):
        return False  
    with open(list_file, 'r') as file:
        return any(line.strip() == folder_name for line in file)



def main():
    config = functions.load_config()
    if config['debugging_comments']:
        print('\n\n\nStarting script _101_main_download_sentinel_2')
    bounding_box = functions.get_bounding_box(config)
    inputs_folder = config['download_and_processfolder']

    if not os.path.exists(inputs_folder):
        os.mkdir(inputs_folder) 
        if config['debugging_comments']:
            print(f"Creating {inputs_folder}...")
    else:
        if config['debugging_comments']:
            print(f"folder {inputs_folder} already exists")

        

    #check if boundingbox is correct
    if isinstance(bounding_box, list):
        # Check if the bounding_box is a list of coordinates
        if all(isinstance(coord, list) and len(coord) == 2 for coord in bounding_box):
            pass
        else:
            raise ValueError("Invalid bounding box coordinates")
    else:
        raise ValueError("Invalid bounding box format")

    #get login data 
    token = authorize(config['email'], config['password'])

    #get start and enddate depending on config. 
    #Function also checks if there is allready a snowpass2 file and waits for user input if so.
    #for more info see function file
    start_date, end_date = functions.get_start_and_endate(config, inputs_folder)

    cloud_cover = config['cloud_cover']
    print(start_date)
    print(end_date)
    products = get_products(start_date, end_date, bounding_box, cloud_cover)
    #print(products)
    '''
    if config['debugging_comments']:
        request_payload = {
        "start_date": start_date,
        "end_date": end_date,
        "bounding_box": bounding_box,
        "cloud_cover": cloud_cover
        }
        print(json.dumps(request_payload, indent=4))  # Print the payload
    '''
    if not products["value"]:
        print("no products")
        return

    #go thorugh downlaodable products and download them 
    for index in range(len(products["value"])):
    # for index in [6]:
        token = authorize(config['email'], config['password'])
        product_id = products["value"][index]["Id"]
        product_name = products["value"][index]["Name"]
        content_length = products["value"][index]["ContentLength"]
        origin_date = products["value"][index]["OriginDate"]
        #print(product_id,product_name)
        folder_name = f"{inputs_folder}/{origin_date.replace(':','-')}_{product_id}"

       # Fist test if there is no such folder and it does not delete the folders after merging
       # or (second test) if it deletes after merging and the folder is also not in the list or still existing (still existing happens if the images did not get merged for whatever reason)
        if not os.path.exists(folder_name) and (not config['delete_downloaded_folders_after_merging'] or (config['delete_downloaded_folders_after_merging'] and not is_folder_in_list(folder_name, os.path.join(config['download_and_processfolder'],config['info_file'])))):
            os.mkdir(folder_name)
            print(f"Downloading image {index + 1} / {len(products['value'])} Image date: {origin_date.split('T')[0]}")
            try:
                download_product(token, product_id, product_name, content_length)
            except Exception as e:
                print(f"Failed to download {product_name}. Error: {e}")
        else:
            print(f"File {index + 1} / {len(products['value'])} already exists. Skipping download.")
            continue

                

        granule_path = f"{product_name}/GRANULE"
        img_path = f"{granule_path}/{os.listdir(granule_path)[0]}"

        R10_path = f"{img_path}/IMG_DATA/R10m"
        R10_files = ["B03", "B04", "B08"]
        for i in os.listdir(R10_path):
            for j in R10_files:
                if i.split("_")[2] == j:
                    shutil.copy(f"{R10_path}/{i}", f"{folder_name}/{j}.jp2")

        R20_path = f"{img_path}/IMG_DATA/R20m"
        R20_files = ["B11"]
        for i in os.listdir(R20_path):
            for j in R20_files:
                if i.split("_")[2] == j:
                    shutil.copy(f"{R20_path}/{i}", f"{folder_name}/{j}.jp2")

        shutil.copy(f"{img_path}/QI_DATA/MSK_CLDPRB_20m.jp2", f"{folder_name}/CLD.jp2")
        
        for file in os.listdir(product_name):
            if file.startswith("MTD_") and file.endswith(".xml"):
                shutil.copy(os.path.join(product_name, file), f"{folder_name}/{file}")

        shutil.rmtree(product_name)

    return


if __name__ == "__main__":
    functions.remove_dot_config()
    main()