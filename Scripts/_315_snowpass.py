import rasterio
import numpy as np
import os

def main(folder_path, value_red, value_ndsi, output_name):
    cloud_class = os.path.join(folder_path, 'cloud_class.tiff')
    NDSI = os.path.join(folder_path, 'NDSI.tiff')
    red_b4 = os.path.join(folder_path, 'B04.tiff')
    output_file = os.path.join(folder_path, output_name)

    # Open cloud_class band
    with rasterio.open(cloud_class) as cc_src:
        cc = cc_src.read(1)

    # Open NDSI band
    with rasterio.open(NDSI) as ndsi_src:
        ndsi = ndsi_src.read(1)

    # Open RED band
    with rasterio.open(red_b4) as red_src:
        red = red_src.read(1)
        profile = red_src.profile.copy()  # Copy the profile



    # Claculate Snowpass: 1 -> No snow, 2 -> Snow, 0 -> no image (the value lower then -0.09 are no image)
    snowpass_1 = np.where(
        (cc < 2) & (red.astype(float) > value_red) & (ndsi.astype(float) > value_ndsi),
        2, #snow cc < 2 also pixels from cloudclas1 can be classified but in the final product it gest overwritten by the cloudvalue that is set in config
        np.where(
            red.astype(float) < (-0.09), #no image
            0,
            1
        )
    )

    # Debugging: Print snowpass_1 values
    #print("snowpass_1 values:")
    #print(snowpass_1)

    profile.update(dtype=rasterio.uint8, count=1)


    with rasterio.open(output_file, 'w', **profile) as dst:
        dst.write(snowpass_1.astype(rasterio.uint8), 1)


    print(f"snowpass calculation complete. Output saved to {output_file}")

if __name__ == "__main__":
    functions.remove_dot_config()
    main(folder_path)
