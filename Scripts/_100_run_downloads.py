import functions
import yaml
import os
import time
import _101_main_download_sentinel_2
import _102_forest_download


def main():
    config = functions.load_config()
    dem_folder = os.path.join(config['static_path'], config['dem_downloadfolder'])
    #Warns the user that DEM files have to be downlaoded manually. And recoments a certain homepage for that from a certain homepage
    print('')
    print('NOTE: DEMs have to be downloadaed manualy from the area of intrest from https://sonny.4lima.de/ and placed in the folder: ', dem_folder)
    print('Script asumes that you did this! Abort the script if you did not downlaod DEMs manually. Ctrl + C does the job.')
    print('')
    time.sleep(4)

# Downloads Satelite images 
    _101_main_download_sentinel_2.main()

    _102_forest_download.main()

    if config['debugging_comments']:
        print('Downlaods finished!')
    
if __name__ == "__main__":
    functions.remove_dot_config()
    main()
